# C# Fundamentals

<!-- TOC -->

- [Properties](#properties)
- [Generics](#generics)
    - [Generic Constraints](#generic-constraints)
- [Delegates](#delegates)
    - [Func Delegates](#func-delegates)
    - [Action Delegates](#action-delegates)
    - [Predicate Delegates](#predicate-delegates)
- [Events](#events)
- [Operator Overloading](#operator-overloading)
- [Indexers](#indexers)
- [User-Defined Conversions](#user-defined-conversions)
- [Extension Methods](#extension-methods)
- [Dynamics](#dynamics)
- [Lambdas](#lambdas)
- [LINQ](#linq)
    - [LINQ Syntaxes](#linq-syntaxes)
        - [Query Syntax](#query-syntax)
        - [Method Syntax](#method-syntax)
    - [Standard Query Operators](#standard-query-operators)
    - [Expressions](#expressions)
    - [Expression Trees](#expression-trees)
- [Reflection](#reflection)
- [Nullable Types](#nullable-types)
- [Regular Expressions](#regular-expressions)
- [Multi-Threaded & Asynchronous Programming](#multi-threaded--asynchronous-programming)
    - [Threading](#threading)
        - [Thread Safety and Locking](#thread-safety-and-locking)
        - [Mutexes](#mutexes)
    - [Asynchronous Programming](#asynchronous-programming)
        - [Tasks](#tasks)
        - [Async & Await](#async--await)
    - [Parallel Class](#parallel-class)
        - [Data Parallelism](#data-parallelism)
        - [Task Parallelism](#task-parallelism)
    - [Multi-Threading Tips](#multi-threading-tips)
        - [Create Local Variables to Avoid Race Conditions](#create-local-variables-to-avoid-race-conditions)
        - [Use Interlocked Methods to Prevent Simultaneous Access](#use-interlocked-methods-to-prevent-simultaneous-access)
- [Covariance and Contravariance](#covariance-and-contravariance)
- [Serialization and Marshaling](#serialization-and-marshaling)
    - [Serialization](#serialization)
    - [Marshaling](#marshaling)
- [Testing](#testing)
- [Appendix A: LINQ Standard Query Operators](#appendix-a-linq-standard-query-operators)
    - [SQ: Where](#sq-where)
    - [SQ: OfType](#sq-oftype)
    - [SQ: OrderBy](#sq-orderby)
    - [SQ: OrderByDescending](#sq-orderbydescending)
    - [SQ: ThenBy](#sq-thenby)
    - [SQ: ThenByDescending](#sq-thenbydescending)
    - [SQ: Reverse](#sq-reverse)
    - [SQ: GroupBy](#sq-groupby)
    - [SQ: ToLookup](#sq-tolookup)
    - [SQ: GroupJoin](#sq-groupjoin)
    - [SQ: Join](#sq-join)
    - [SQ: Select](#sq-select)
    - [SQ: SelectMany](#sq-selectmany)
    - [SQ: Aggregate](#sq-aggregate)
    - [SQ: Average](#sq-average)
    - [SQ: Count](#sq-count)
    - [SQ: LongCount](#sq-longcount)
    - [SQ: Max](#sq-max)
    - [SQ: Min](#sq-min)
    - [SQ: Sum](#sq-sum)
    - [SQ: All](#sq-all)
    - [SQ: Any](#sq-any)
    - [SQ: Contains](#sq-contains)
    - [SQ: ElementAt](#sq-elementat)
    - [SQ: ElementAtOrDefault](#sq-elementatordefault)
    - [SQ: First](#sq-first)
    - [SQ: FirstOrDefault](#sq-firstordefault)
    - [SQ: Last](#sq-last)
    - [SQ: LastOrDefault](#sq-lastordefault)
    - [SQ: Single](#sq-single)
    - [SQ: SingleOrDefault](#sq-singleordefault)
    - [SQ: Distinct](#sq-distinct)
    - [SQ: Except](#sq-except)
    - [SQ: Intersect](#sq-intersect)
    - [SQ: Union](#sq-union)
    - [SQ: Skip](#sq-skip)
    - [SQ: SkipWhile](#sq-skipwhile)
    - [SQ: Take](#sq-take)
    - [SQ: TakeWhile](#sq-takewhile)
    - [SQ: Concat](#sq-concat)
    - [SQ: SequenceEqual](#sq-sequenceequal)
    - [SQ: DefaultEmpty](#sq-defaultempty)
    - [SQ: Empty](#sq-empty)
    - [SQ: Range](#sq-range)
    - [SQ: Repeat](#sq-repeat)
    - [SQ: AsEnumerable](#sq-asenumerable)
    - [SQ: AsQueryable](#sq-asqueryable)
    - [SQ: Cast](#sq-cast)
    - [SQ: ToArray](#sq-toarray)
    - [SQ: ToDictionary](#sq-todictionary)
    - [SQ: ToList](#sq-tolist)
- [Appendix B: Regular Expressions](#appendix-b-regular-expressions)
    - [Escaped Characters](#escaped-characters)
    - [Anchors](#anchors)
    - [Quantifiers](#quantifiers)
    - [OR Operator](#or-operator)
    - [Character Classes](#character-classes)
    - [Flags](#flags)
    - [Grouping and Capturing](#grouping-and-capturing)
    - [Bracket Expressions](#bracket-expressions)
    - [Greedy and Lazy Matching](#greedy-and-lazy-matching)
    - [Boundaries](#boundaries)
    - [Back-References](#back-references)
    - [Look-Ahead & Look-Behind](#look-ahead--look-behind)

<!-- /TOC -->

## Properties

Properties are something like a hybrid of methods and fields. They can be setup to have `get` and, optionally, `set` "methods" to interact with another (usually private variable). Either of these methods can be customized with multiple operations.

- An equivalent to properties in something like C++ would be `public` class functions: `GetVar()` and `SetVar(int i)`.

Example:

```Csharp
private int _num;

public int Num
{
    get     // default, returns Num
    {
        return _num;
    }
    set     // stores absolute value of input value
    {
        if (value < 0)
        {
            _num = -1 * value;
        }
        else
        {
            _num = value;
        }
    }
}
```

## Generics

Generics are very similar to templates in C++. They provide a way to create classes and methods without knowing the types ahead of time, but while still retaining type safety. They can be created somewhat similar to C++ templates:

```Csharp
public class GenericContainer<T>
{
    private T[] items;
}
```

### Generic Constraints

While C# does not have a feature for generics equivalent to C++ template specialization, it does have a few options for making them more specific. Generics, like any other method, can be overridden. Additionally, constraints can be placed on generics to restrict them to only accept certain types. This is done by appending a `where` clause to the generic declaration. The following examples show the various ways to constrain generics:

```Csharp
// Only accept when T is derived from BaseClass
public class GenericContainer<T> where T : BaseClass {}

// Only accept when T uses MyInterface
public class GenericContainer<T> where T : MyInterface {}

// Only accept when T is a class
public class GenericContainer<T> where T : class {}

// Only accept when T is a struct
public class GenericContainer<T> where T : struct {}

// Only accept when T has a constructor with param1 and param2 parameters
public class GenericContainer<T> where T : new(param1, param2) {}
```

## Delegates

Delegates act like variable types, but instead of a value, they hold a method. Delegates are declared like methods and can be assigned as any method with the same parameters and return types.

Example:

```Csharp
// delegate that handles all methods that are given a single int and return a string
public delegate string GetStringDelegate(int i);

public void Main()
{
    GetStringDelegate strDelegate = GetFirstName;
    string firstName = strDelegate(4);  // assigns to the FirstName of person #4

    strDelegate = GetLastName;
    string lastName = strDelegate(2);   // assigns to the LastName of person #2
}
```

As delegates are a type (i.e. a built-in class), they also have fields, methods, and properties associated with them. You can, for example get the name of the method that a delegate instance is currently assigned to:

```Csharp
MethodInfo mi = strDelegate.Method;
Console.WriteLine(mi.Name);     // prints the method name: "GetFirstName"
```

Delegates can also be assigned anonymous methods:

```Csharp
delegate(Person p)
{
    return p.Age > 20;
};
```

**NOTE:** since C# 3.0, [lambda functions](#lambdas) are much preferred for creating anonymous functions.

There are special built-in types of delegates that can be used to accomplish certain tasks, these are `Func`, `Action`, `Predicate` delegates.

### Func Delegates

`Func` delegates are generic delegates that have zero or more input parameters and one output parameter:

```Csharp
public int Sum(int i, int j)
{
    return i + j;
}

Func<int, int, int> func = Sum;
Console.WriteLine(func(3, 5));  // 8
```

### Action Delegates

`Action` delegates are similar to Func delegates except that they have no return value (function returning `void` can be assigned to them).

```Csharp
public void PrintNum(int i)
{
    Console.WriteLine(i + 5);
}

Action<int> func = PrintNum;
func(45);   // 50
```

### Predicate Delegates

`Predicate` delegates are similar to Func delegates except that their return type is always `bool`.

## Events

Events allow classes to notify other classes when something happens. Events hold delegates that have the information for the event as parameters.

Events are defined using the `event` keyword:

```Csharp
public event DelegateName EventName;
```

While custom delegates can be used with an event, it is typically sufficient to use the .NET standard `EventHandler` delegate. This delegate operates for a return type of `void` and parameters of an `object` and an `EventArgs` class object.

To attach an `EventHandler` (that matches the delegate) to an event the `+=` operator is used:

```Csharp
Class.EventName += EventHandler;
```

- The `-=` operator is used to detach an `EventHandler`

## Operator Overloading

Like other statically typed languages, C# allows for operator overloading, that is, the overriding of the behavior of an operator symbol based on the context in which it is used.

The following operators can be overloaded in C#:

- Unary operators (`+`, `-`, `!`, `~`, `++`, `--`, `true`, `false`)
- Binary operators (`+`, `-`, `*`, `/`, `%`, `&`, `|`, `^`, `<<`, `>>`)
- Comparison operators (`==`, `!=`, `<`, `>`, `<=`, `>=`)
    - **Note:** Comparison operators must be overloaded in pairs (`==` and `!=`, etc.)

Assignment operators such as `=` and `+=` cannot be overloaded, however the first symbol inherits any overloaded behavior (example: `+=` inherits from `+`)

The `()` operator and `[]` operator cannot be overloaded, however the `[]` operator can be defined as an indexer (see [Indexers](#indexers))

Operators are overloaded in the following way:

```Csharp
// Overloads must be public and static!
public static RetType operator +(InType1 var1, InType2 var2)
{
    // Do something and return a value
}
```

## Indexers

As mentioned, the `[]` operator cannot technically be overloaded in C#. There is, however, a language feature that provides a function very similar to this known as indexers. Indexers allow accessing class objects as an index of a class instance.

While this is not very useful for most classes, there are some applications where it can be more convenient to use indexes rather than full names. Indexers are treated like properties as they have `get` and `set` methods.

```Csharp
public int this[int index]
{
    get
    {
        switch (index)
        {
            case 0:
                return this.var0;
            case 1:
                return this.var1;
            default:
                throw new IndexOutOfRangeException();
        }
    }
    set
    {
        switch (index)
        {
            case 0:
                this.var0 = value;
                break;
            case 1:
                this.var1 = value;
                break;
            default:
                throw new IndexOutOfRangeException();
                break;
        }
    }
}
```

## User-Defined Conversions

While some implicit and explicit conversions for basic types exist for built-in types and containers, custom classes, structs, and objects may require custom conversions. In C# user-defined conversions can be created and deemed `implicit` or `explicit`. The syntax for these conversions is similar to that of operator overloading:

```Csharp
public static implicit operator CustomClass(int value)
{
    return new CustomClass() { Number = value };
}
```

## Extension Methods

In C#, extension methods allow methods to be added to outside classes, allowing the extension of non-user classes.\
The extension method must be inside a non-nested, non-generic, `static` class, and have a parameter with a `this` prefix.

For example, to add a method that counts the number of occurrences of a character in a string:

```Csharp
public static class Extensions
{
    public static int CountChar(this String str, char iChar)
    {
        int charCount = 0;
        foreach (char c in str)
        {
            if (c == iChar)
            {
                charCount++;
            }
        }
        return charCount;
    }
}

string myStr = "This is an example string";
Console.WriteLine(myStr.CountChar('s'));    // 3
```

## Dynamics

By using the `dynamic` type, a variable can be changed at runtime without casting. While the `var` type is also an inferred type, it is set once and cannot be changed (must be casted). `dynamic` variables are similar to those used by Python.

## Lambdas

Lambda expressions allow for the creation of anonymous functions that can be used in a variety of situations, even as input parameters.\
Lambdas are an essential part of [LINQ](#linq), a set of features and functions added to .NET by Microsoft to allow for a query-like way to interact with objects.

All lambda expressions in C# use the lambda operator `=>` (aka "goes to" or "becomes"). The left side of a lambda expression defines the input parameters, while the right side can be considered the body of the expression.

A simple example of a lambda expression would be

```Csharp
x => (x % 2) == 0;
```

Which would be equivalent to:

```Csharp
bool isEven (int x)
{
    return (x % 2) == 0;
}
```

In this way, lambdas can often be much more succinct, at the cost of a slight hit to readability, especially for those who are unfamiliar with lambda expressions.

Lambda expressions don't require parameters...

```Csharp
() => Console.WriteLine("Hello");
```

... and can also accept multiple parameters (and also input types)...

```Csharp
(Person x, Person y) => x.Age > y.Age;
```

... and can have multiple lines and local variables:

```Csharp
x =>
{
    int someAge = 45;
    return x.Age < someAge;
}
```

As mentioned earlier, lambda expressions can also be used as input parameters to methods, and are compatible with iterating operations:

```Csharp
List<int> allNumbers = new List<int>() { 1, 2, 3, 4, 5 };
List<int> evenNumbers = allNumbers.FindAll(x => (x % 2) == 0);
```

Lambdas can also be used to select items without a for loop:

```Csharp
List<string> names = contacts.Select(x => x.Name);
```

As lambdas are anonymous functions, they can also create anonymous types:

```Csharp
var peopleList = contacts.Select(x => new { Name = x.Name, Age = x.Age });
```

## LINQ

LINQ (**L**anguage **In**tegrated **Q**uery) is a query-like syntax for .NET to access data from a variety of sources. LINQ can be used to access internal data (from object that implement `IEnumerable` or `IQueryable` interfaces), or external data such as SQL databases or web services.

### LINQ Syntaxes

LINQ can access data sources using two main types of syntax: query and method.\
The use of query vs. method syntax is purely cosmetic, as the compiler treats them the exact same.

#### Query Syntax

Query syntax is written much like SQL. For example, to select all items in an `IEnumerable` (in this case, a list of people) that have a certain property (their age), the following query would work:

```Csharp
var results = from p in personList where p.Age > 45 select p;
```

#### Method Syntax

The method syntax involves [extension methods](#extension-methods) applied to `IEnumerable` and `IQueryable` objects. To get the same result for the previous example using method syntax one could write:

```Csharp
var results = personList.Where(s => s.Age > 45).ToArray();
```

Notice the use of the [lambda function](#lambdas), these are an important part of using LINQ.

### Standard Query Operators

Standard Query Operators are operators for LINQ that are built-in to both the query and method syntaxes. They are implemented as extension methods from the `System.Link` namespace. While one can create their own extension methods for `IEnumerable` and `IQueryable` objects, the standard provides a lot of common query language operations.

| Classification | Standard Query Operators                                                                                                                                                                                                                               |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Filtering      | [Where](#sq-Where), [OfType](#sq-OfType)                                                                                                                                                                                                               |
| Sorting        | [OrderBy](#sq-OrderBy), [OrderByDescending](#sq-OrderByDescending), [ThenBy](#sq-ThenBy), [ThenByDescending](#sq-ThenByDescending), [Reverse](#sq-Reverse)                                                                                             |
| Grouping       | [GroupBy](#sq-GroupBy), [ToLookup](#sq-ToLookup)                                                                                                                                                                                                       |
| Join           | [GroupJoin](#sq-GroupJoin), [Join](#sq-Join)                                                                                                                                                                                                           |
| Projection     | [Select](#sq-Select), [SelectMany](#sq-SelectMany)                                                                                                                                                                                                     |
| Aggregation    | [Aggregate](#sq-Aggregate), [Average](#sq-Average), [Count](#sq-Count), [LongCount](#sq-LongCount), [Max](#sq-Max), [Min](#sq-Min), [Sum](#sq-Sum)                                                                                                     |
| Quantifiers    | [All](#sq-All), [Any](#sq-Any), [Contains](#sq-Contains)                                                                                                                                                                                               |
| Elements       | [ElementAt](#sq-ElementAt), [ElementAtOrDefault](#sq-ElementAtOrDefault), [First](#sq-First), [FirstOrDefault](#sq-FirstOrDefault), [Last](#sq-Last), [LastOrDefault](#sq-LastOrDefault), [Single](#sq-Single), [SingleOrDefault](#sq-SingleOrDefault) |
| Set            | [Distinct](#sq-Distinct), [Except](#sq-Except), [Intersect](#sq-Intersect), [Union](#sq-Union)                                                                                                                                                         |
| Partitioning   | [Skip](#sq-Skip), [SkipWhile](#sq-SkipWhile), [Take](#sq-Take), [TakeWhile](#sq-TakeWhile)                                                                                                                                                             |
| Concatenation  | [Concat](#sq-Concat)                                                                                                                                                                                                                                   |
| Equality       | [SequenceEqual](#sq-SequenceEqual)                                                                                                                                                                                                                     |
| Generation     | [DefaultEmpty](#sq-DefaultEmpty), [Empty](#sq-Empty), [Range](#sq-Range), [Repeat](#sq-Repeat)                                                                                                                                                         |
| Conversion     | [AsEnumerable](#sq-AsEnumerable), [AsQueryable](#sq-AsQueryable), [Cast](#sq-Cast), [ToArray](#sq-ToArray), [ToDictionary](#sq-ToDictionary), [ToList](#sq-ToList)                                                                                     |

See [Appendix A](#appendix-a-linq-standard-query-operators) for more on the individual Standard Query Operators, or click the appropriate links above.

### Expressions

With LINQ, a new type is introduced called an `Expression`. An expression is essentially a strongly-typed lambda expression. Assigning a lambda expression to an `Expression<TDelegate>` compiles the expression not into executable code, but to an [expression tree](#expression-trees), that can be used by external LINQ providers.

Expressions can be created by wrapping a delegate in the `Expression<>` generic:

```Csharp
Expression<Func<int, bool>> isEvenExpr = n => (n % 2) == 0;
```

An expression cannot be directly invoked as it is stored to the [expression tree](#expression-trees) rather than the executable code. To add the expression to the executable dynamically, the expression can be compiled and assigned back to a delegate:

```Csharp
// compile
Func<int, bool> isEven = isEvenExpr.Compile();

// then invoke
bool result = isEven(5);    // false
```

### Expression Trees

An expression tree is simply a data structure (tree-like, naturally) that holds expressions. Expression trees are kept in memory and hold the expressions themselves, but not the result.

## Reflection

Reflection allows for obtaining type information at runtime. By using reflection, new types, values and methods can be introduced at runtime.\
For example, to change a variable by inputting its name:

```Csharp
class MyClass
{
    private static int a = 5, int b = 10, int c = 20;
}

string varName = "b";
Type t = typeof(MyClass);
FieldInfo fi = t.GetField(varName, BindingFlags.NonPublic | BindingFlags.Static);
if (fi != null)
{
    int tmp = fi.GetValue(null);
    fi.SetValue(null, tmp + 1);
}
```

In another example, a new class instance can be created dynamically and have a method called:

```Csharp
class MyClass
{
    private int _num = 42;

    public void PrintSum(int n)
    {
        Console.WriteLine(n + 42);
    }
}

Type type1 = typeof(MyClass);

// Find the default constructor
ConstructorInfo ctor = type1.GetConstructor(Type.EmptyTypes);

// if default constructor exists, execute PrintSum(10)
if (ctor != null)
{
    object instance = ctor.Invoke(null);
    MethodInfo mi = type1.GetMethod("PrintSum");
    mi.Invoke(instance, new object[] { 10 });
}
```

## Nullable Types

By using the `Nullable<>` generic, any type can accept `null` as a value:

```Csharp
Nullable<int> i = null;
```

Additionally, the `?` operator can be used as shorthand. The `??` operator can also be used to allow converting to a non-nullable type:

```Csharp
int? i = null;
int j;  // non-nullable

j = i ?? 0; // if 'i' is null, then assign 'j' to 0
```

Normally, comparing a value with something assigned to 'null' will throw an exception, but with the `Nullable` class, a comparison can be made:

```Csharp
bool b1 = i < j;    // throws null exception
bool b2 = Nullable.Compare<int>(i, j) < 0;  // true
```

## Regular Expressions

As with a lot of languages, C# has built-in support for regular expressions. While this section covers the C# specific classes and methods that can be used to parse regular expressions, a review of regular expressions themselves can be found in [Appendix B](#appendix-b-regular-expressions).

The .NET regular expression library can be used from the `System.Text.RegularExpressions` namespace, the `Regex` class, in particular. This class provides a few useful methods:

| Method    | Description                                                     |
| --------- | --------------------------------------------------------------- |
| `Match`   | Returns the first occurrence of a regular expression            |
| `Matches` | Returns a collection of all occurrences of a regular expression |
| `IsMatch` | Returns a `bool` indicating whether a match exists              |
| `Replace` | Replace all occurrences of a regular expression with a string   |
| `Split`   | Split a string at the occurrences of a regular expression       |

For example, to find all occurrences of an email address in a List of strings:

```Csharp
// use '@ prefix for verbose string
string emailRegex = @"/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/";
var emails = Regex.Matches(emailList, emailRegex);

foreach (var email in emails)
{
    Console.WriteLine(email.Value);
}
```

## Multi-Threaded & Asynchronous Programming

### Threading

Threading is an important, albeit difficult, topic for any modern language and C# is no exception.

**Note:** While threading can be very effective when done correctly, it is not always safe or easy to understand. For a simpler way to handle multi-tasking see [Asynchronous Programming](#asynchronous-programming).

To use threads in C#, the `System.Threading` namespace should be used. Threads can then be created and used with code like the following:

```Csharp
Thread thr1 = new Thread(MethodName); // Creates new thread that calls MethodName
thr1.Start(); // Starts new thread
thr1.Join();  // Waits for thr1 to complete before continuing to next line
```

**Note:** Threaded methods are typically `void` returning and parameter-less. Parameters can be added using [lambdas](#lambdas), and return behavior can be emulated with `out` parameters:

```Csharp
Thread thr2 = new Thread(() => ParamMethod(param1, param2, out retVal));
```

#### Thread Safety and Locking

While some code is inherently thread-safe, other operations may require thread locking. One pattern for this involves using the `lock` keyword and a private `object`:

```Csharp
// class MyClass
private object threadLock = new object();

lock(threadLock)
{
    // Only one thread can operate on this block at a time
}
```

#### Mutexes

An alternative to locked objects come in the form of the `Mutex` class. A mutex can be used across multiple processes as it is held by the operating system. A mutex can be instantiated, and then its `WaitOne` method can be called to lock the mutex, with its `ReleaseMutex` method unlocking the mutex. Mutexes are also automatically released when disposed or closed.

```Csharp
private static Mutex mutex = new Mutex();

public void DoStuff(int num)
{
    mutex.WaitOne(); // wait until mutex is released
    sum += num;
    mutex.ReleaseMutex(); // release mutex
}
```

### Asynchronous Programming

Introduced in C# 5, asynchronous programming is a feature set that makes multi-tasking and avoiding bottlenecks easier, and while it does not replace [threading](#threading), it does make programming this way much easier, safer, and simpler. Asynchronous programming does not necessarily involve creating new threads, but it has the ability to when needed.

#### Tasks

While creating threads can be useful when you need absolute control over it, a more practical way of multi-tasking comes with using tasks. A task can be considered a promise for a future result, for example a `Task<T>` promises to generate an object of type `T` at _some_ point. Tasks can be more efficient than creating threads as they can use the Thread Pool that .NET already provides.

Performing a simple task can be done by creating a new instance of the `Task` class, and passing it an [Action delegate](#action-delegates).

```Csharp
Task task1 = new Task(new Action<int>(PrintNum))

task1.Start();
```

For simple or one-time tasks, anonymous functions in the form of [lambda expressions](#lambdas) can be used. Tasks can also be run right away using the `Task.Factory.StartNew` method:

```Csharp
Task.Factory.StartNew(() => Console.WriteLine("New Task!"));
```

Tasks can also be created as instances, often as a return type in a `Task` method:

```Csharp
public static Task<float> Halve(int i)
{
    return Task.Run<float>(() =>
    {
        return i * 0.5f;
    });
}
```

#### Async & Await

Most asynchronous programming in C# involves two keywords: `async` and `await`.

By using the `async` keyword in a method's definition, the compiler knows to run the method asynchronously (if it can).

```Csharp
public async void DoSomething()
{
    // does something
}
```

Within an `async` method, the `await` keyword can be used to wait for a method to finish before continuing. The method called, however, must return an awaitable type such as a [Task](#tasks).

```Csharp
public async void CallHalve()
{
    float result = await Halve(5);
    Console.WriteLine(result);  // 2.5f
}
```

Multiple tasks can be awaited, either by having multiple `await` statements, or by using the `Task.WhenAll` method:

```Csharp
Task<float> t1 = Halve(9);
Task<float> t2 = Halve(12);

await Task.WhenAll(t1, t2);
```

### Parallel Class

As of .NET 4.5, a `Parallel` class exists to abstract threading. Parallelism uses all available CPU resources automatically and is very simple to use.

#### Data Parallelism

Parallelism can be used to go through large collections of data using built in methods that replace common iterations and loops. The most commonly used of these methods being `Parallel.For` and `Parallel.ForEach`:

```Csharp
var result = Parallel.For(0, 1000, async (int i, ParallelLoopState pls) =>
{
    Console.WriteLine(i);

    // wait 10 ms
    await Task.Delay(10;);

    // equivalent to break in for loop
    if (i > 500)
    {
        pls.Break();
    }
});
```

#### Task Parallelism

Parallelism can also be used to run multiple tasks in parallel using the `Parallel.Invoke` method. This method takes an array of [Action delegates](#action-delegates) as a parameter (can also use lambdas).

```Csharp
Parallel.Invoke(
    () =>
    {
        Console.WriteLine("Do this...");
    },
    () =>
    {
        Console.WriteLine("... but also do this!");
    });
```

### Multi-Threading Tips

#### Create Local Variables to Avoid Race Conditions

Make sure to avoid race conditions. This can come from a variety of sources, but one of the most common is using the index value in a for loop:

```Csharp
for (int i = 0; i < 5; ++i)
{
    // Possibly outputs "55555"
    Task.Factory.StartNew(() => { Console.Write(i); Thread.Sleep(50); });
}
```

Because the index `i` may increment before the thread is finished, the threads will print unexpected values and have a high chance of printing `i` after the loop is complete, resulting in `55555`.\
To fix this, a local variable can be created and assigned to the value of `i` to allow the thread to keep it in its stack:

```Csharp
for (int i = 0; i < 5; ++i)
{
    int copy = i;
    // outputs some permutation of "01234" as expected
    Task.Factory.StartNew(() => { Console.Write(copy); Thread.Sleep(50); });
}
```

#### Use Interlocked Methods to Prevent Simultaneous Access

Using a `Parallel.For` loop (see [Data Parallelism](#data-parallelism)), we can quickly sum up calculated values:

```Csharp
int sum = 0;
Parallel.For(0, 100, async (int i) =>
{
    sum += myArray[i];
});
```

This may, however, be an issue as multiple threads could try to change `sum` at the same time. To avoid this, the `Interlocked` static class provides methods for thread/task shared variables:

```Csharp
int sum = 0;
Parallel.For(0, 100, async (int i) =>
{
    Interlocked.Add(ref sum, myArray[i]);
});
```

## Covariance and Contravariance

Covariance and contravariance describe the ability of C# methods, delegates, and generics to accept a derived class when a base class is expected.

**Covariance** allows a derived object to be assigned to something that expects a base class of the object:

```Csharp
List<BaseClass> myList = new List<BaseClass>();
myList.Add(new BaseClass());
// Covariance
myList.Add(new DerivedClass());
```

Covariance applies to collections (`IEnumerable`), and generic interfaces using an `out` parameter.

**Contravariance** provides the opposite effect: base objects can be passed to something that expects a derived class.

```Csharp
Action<BaseClass> action1 = x => Console.WriteLine(x.Name);
// Contravariance
Action<Derived> action2 = action1;
```

Contravariance applies to delegates (`Action` and `Func`), and generic interfaces using an `in` parameter.

## Serialization and Marshaling

While C# has a lot of built-in interoperability, thanks to being a part of the .NET framework, there will still be cases where data will need to be shared, transferred, or stored in a compatible manner. That is where serialization and marshaling come into play.

### Serialization

Serialization is the converting of code objects to a portable format. Serialization is typically done by writing to files in either a binary, XML, or JSON format. By saving these files, they can later be imported.\
An example serializing a class to XML:

```Csharp
public class Contact
{
    public string Name;
    public string Address;
    public string PhoneNumber;
}

Contact c1 = new Contact() { Name = "Bill", Address = "1234 Maple St", Phone = "(888) 444-6666"};

using (var writer = new StreamWriter("myfile.xml"))
{
    var xs = new XmlSerializer(typeof(Contact));
    xs.Serialize(writer, this);
    writer.Flush();
}
```

Objects can be de-serialized in a similar way:

```Csharp
Contact c2;
using (var reader = File.OpenRead("myfile.xml"))
{
    var xs = new XmlSerializer(typeof(Contact));
    c2 = xs.Deserialize(reader) as Contact;
}
```

### Marshaling

As C# is compiled to an intermediate language and run on the CLR, it is not inherently compatible with un-managed code (i.e. native C++). In order to pass data safely to un-managed code, the .NET framework provides a conversion process known as marshaling.

## Testing

With any project the consists of more than a few functions, especially code that is highly modular and developed by multiple contributors, tests should be set up to make sure the code is functioning. Unit tests, in particular are useful.\
Like many popular languages, C# provides a plethora of ways to implement testing. Built-in to Visual Studio is MSTest, the Microsoft test framework. This is the framework that will be used for this section, though NUnit and xUnit are also popular.

For this example a simple class with a few basic methods will be tested:

```Csharp
public class ExampleClass
{
    public int AddNums(int i1, int i2)
    {
        return i1 + i2;
    }

    public int SubNums(int i1, int i2)
    {
        // Error that could lead to some headaches later on!
        return i1 + i2;
    }
}
```

To write a test, a test class must be created. To indicate a test class, the `[TestClass]` attribute is used. Each test class should also have test methods, indicated by a `[TestMethod]` attribute. A test method must also have no parameters and return `void`.\
Within a test method, an `Assert` method is used to indicate success or failure of a test.\
Here is the example test class for our `ExampleClass`:

```Csharp
using ExampleClass;

[TestClass]
public class MyTest1
{
    [TestMethod]
    public void Test_AddNums()
    {
        ExampleClass ex = new ExampleClass();
        int result = ex.AddNums(9, 4);
        Assert.AreEqual(result, 15);
    }

    [TestMethod]
    public void Test_SubNums()
    {
        ExampleClass ex = new ExampleClass();
        int result = ex.SubNums(9, 4);
        Assert.AreEqual(result, 5);     // This test fails!
    }
}
```

Tests can also be automatically generated by tools like Visual Studio.

Using tests to develop code is a good practice, and often creating tests and requirements before coding can help keep code clean and functional.

## Appendix A: LINQ Standard Query Operators

### SQ: Where

### SQ: OfType

### SQ: OrderBy

### SQ: OrderByDescending

### SQ: ThenBy

### SQ: ThenByDescending

### SQ: Reverse

### SQ: GroupBy

### SQ: ToLookup

### SQ: GroupJoin

### SQ: Join

### SQ: Select

### SQ: SelectMany

### SQ: Aggregate

### SQ: Average

### SQ: Count

### SQ: LongCount

### SQ: Max

### SQ: Min

### SQ: Sum

### SQ: All

### SQ: Any

### SQ: Contains

### SQ: ElementAt

### SQ: ElementAtOrDefault

### SQ: First

### SQ: FirstOrDefault

### SQ: Last

### SQ: LastOrDefault

### SQ: Single

### SQ: SingleOrDefault

### SQ: Distinct

### SQ: Except

### SQ: Intersect

### SQ: Union

### SQ: Skip

### SQ: SkipWhile

### SQ: Take

### SQ: TakeWhile

### SQ: Concat

### SQ: SequenceEqual

### SQ: DefaultEmpty

### SQ: Empty

### SQ: Range

### SQ: Repeat

### SQ: AsEnumerable

### SQ: AsQueryable

### SQ: Cast

### SQ: ToArray

### SQ: ToDictionary

### SQ: ToList

## Appendix B: Regular Expressions

Regular expressions are a way of finding or extracting information quickly from large collections of text. They use a (mostly) standardized syntax to represent different combinations of characters and other sequences. To represent these sequences, the following syntax is used. _Note:_ while surrounding a regex string in `/` is not required, it makes it clear that it is being used as a regular expression and enables certain functions like [Flags](#flags).

### Escaped Characters

Like any syntax, regular expressions have certain operators that must be escaped to be used literally. These include: `^.[]()$|\*+?`. These characters are escaped using a backslash, `\`.

### Anchors

Anchors find text that begins with or ends with the attached text. A `^` represents a beginning anchor, while a `$` represents an end anchor.

```Csharp
"/^First/"            // matches strings that begin with "First"

"/Canada$/"           // matches strings that end with "Canada"

"/^Hello, world$/"    // matches exact string "Hello, world"
```

### Quantifiers

Quantifiers are _similar_ to wildcards but allow for more control over their matches.\
**Note:** Quantifiers are not the same as wildcards, ex: `ab*` will match with "a".

```Csharp
"/abc*/"              // matches string that has "ab" followed by 0 or more 'c'

"/abc+/"              // matches string that has "ab" followed by 1 or more 'c'

"/abc?/"              // matches string that has "ab" followed by 1 or 0 'c'

"/abc{2}/"            // matches string that has "ab" followed by 2 'c'

"/abc{2,}/"           // matches string that has "ab" followed by 2 or more 'c'

"/abc{2,5}/"          // matches string that has "ab" followed by 2 to 5 'c'

"/a(bc)*/"            // matches string that has 'a' followed by 0 or more "bc"
```

### OR Operator

The OR operation can be completed by using a `|` or putting the arguments in square brackets:

```Csharp
"/a(b|c)/"            // matches string that has 'a' followed by 'b' or 'c'

"/a[(bc)(cb)]/"       // matches string that has 'a' followed by "bc" or "cb"
```

### Character Classes

Character classes can be used to match based on the type of character. Character classes are represented by a backslash followed by the letter associated with that class. A capitalized letter negates the match.

```Csharp
"/\d/"                // matches a digit character

"/\w/"                // matches a word character (a-Z and underscore)

"/\s/"                // matches a whitespace character

"/./"                 // matches any character

"/\D/"                // matches any character that is NOT a digit
```

**Note:** character classes and their negated classes are faster than using `.`

### Flags

Flags are values added to the end of a regular expression (after the trailing `/`) to indicate the expression's behavior.

`g` : global - does not return after a match is found, but keeps searching for matches\
`m` : multi-line - `^` and `$` indicate the beginning and end of a line, rather than the whole string\
`i` : insensitive - makes the expression case-insensitive

### Grouping and Capturing

While the `()` operator has been shown earlier as a way to group characters together (think substrings), it also provides a second function: capturing. Capturing "saves" the match of a group and returns it so that it can be displayed or used later. For example, using the string "It is what it is":

```Csharp
"/(it).*(is)/i"
// Matches the entire string, but returns two groups containing "It" and "is"
```

Groups can be made non-capturing by adding a `?:` in the parentheses:

```Csharp
"/(?:it).*(is)/i"
// Matches the entire string, but returns one group containing "is"
```

Groups can also be given names by adding a `?<name>` in the parentheses:

```Csharp
"/(?<first>it).*(?<second>is)/i"
// Matches the entire string, but returns two groups: "first" containing "It" and "second" containing "is"
```

### Bracket Expressions

The `[]` operator has already been shown as an [OR operator](#or-operator), but it also allows for more fine-tuned operations. **Note:** inside a bracket expression, special characters (other than the `^` negator) do not have to be escaped (though they still can be).

```Csharp
"/[abc]/"           // matches string that has 'a', 'b', or 'c'

"/[d-g]/"           // matches string that has 'd', 'e', 'f', or 'g'

"/[a-cx-z]/"        // matches string that has 'a', 'b', 'c', 'x', 'y', or 'z'

"/[^abc]/"          // matches string that DOES NOT have 'a', 'b', or 'c'
```

### Greedy and Lazy Matching

Greedy operators like quantifiers will match as much as they can in a statement. For example, using the string "Sentence 1. Sentence 2.":

```Csharp
"/.+\./"            // matches "Sentence 1. Sentence 2."
```

This may not be the desired behavior. In this example we want to get the sentences as separate matches. To do this, the operators can be made lazy by adding a `?`.

```Csharp
"/.+?\./"          // matches "Sentence 1." and " Sentence 2"
```

**Note:** a better way to handle this would be to avoid using the `.` character class:

```Csharp
"/[^.]+\./"         // matches "Sentence 1." and " Sentence 2"
```

### Boundaries

By surrounding a word or phrase with a `\b`, the regular expression treats the contents as a match word. A match only occurs if the contents have been found and are surrounded by non word characters. A negation, `\B` also exists to return match only text inside a word.

```Csharp
// test string: "cat catatonic concatenate"

"/\bcat\b/"         // matches "cat"

"/\Bcat\B/"         // matches "cat" from "concatenate"
```

### Back-References

Groups can be referenced after their creation using something called back-references. Groups can be referred to by their order (example: `\3` for the third group) or by their name (example: `\k<foo>`)

### Look-Ahead & Look-Behind

Matches can be qualified by additional characters without including them in the match by using look-aheads (`?=`) and look-behinds, (`?<=`). These can be negated by replacing the `=` with a `!`.\
For example:

```Csharp
// test string: "I race my racecar. I'm a good racer!"

"/(?<!\s)race\w*(?=\.)/"    // matches "racecar"
```
