# License Info

**This is not a license, this is only a quick description of the license chosen. This document is not legally binding, to see the actual license, click [here](LICENSE)**

- The license for this project is the Creative Commons Attribution-ShareAlike  (v4.0 Intl.).
- The Attribution clause basically means that if you distribute this project, or a part of this project, give credit to the original author(s). Provide a link to the original project.
- The ShareAlike clause basically means that you cannot add a more restrictive license to a distribution of this project. You can not make this project proprietary, closed-source or put under a more strict license (e.g. non-commercial or no derivative license).
- You are free to modify and redistribute this project as you see fit, but any non-original content should be attributed.