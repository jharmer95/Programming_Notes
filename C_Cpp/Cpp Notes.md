# C++ Notes

<!-- TOC -->

- [Classes](#classes)
    - [Access Modifiers](#access-modifiers)
    - [Constructors](#constructors)
        - [Initialization Lists](#initialization-lists)
    - [Destructors](#destructors)
    - [Default / Delete](#default--delete)
    - [Explicit](#explicit)
    - [Friend](#friend)
    - ["Interfaces"](#interfaces)
    - [Copy Semantics](#copy-semantics)
    - [Conversion Semantics](#conversion-semantics)
- [C++ Keywords](#c-keywords)
    - [Const](#const)
    - [Mutable](#mutable)
    - [Inline](#inline)
    - [Extern](#extern)
    - [Static](#static)
    - [Volatile](#volatile)
    - [Auto and Decltype](#auto-and-decltype)
- [Templates](#templates)
    - [Template Functions](#template-functions)
    - [Template Classes](#template-classes)
    - [Template Specialization](#template-specialization)
        - [Specialized Template Functions](#specialized-template-functions)
        - [Specialized Template Classes](#specialized-template-classes)
    - [C++ Metaprogramming With Templates](#c-metaprogramming-with-templates)
- [Other C++ Features](#other-c-features)
    - [Dynamic Memory](#dynamic-memory)
        - [The `new` operator](#the-new-operator)
        - [The `delete` operator](#the-delete-operator)
    - [References](#references)
    - [Operator Overloading](#operator-overloading)
    - [Functors](#functors)
    - [Enum Classes/Structs](#enum-classesstructs)
    - [Exceptions](#exceptions)
        - [Noexcept](#noexcept)
- [STL](#stl)
    - [STL Containers](#stl-containers)
        - [Sequence Containers](#sequence-containers)
            - [Vectors](#vectors)
            - [Lists](#lists)
            - [Forward Lists](#forward-lists)
            - [Deques](#deques)
            - [STL Arrays](#stl-arrays)
            - [Bitsets](#bitsets)
            - [Valarrays](#valarrays)
        - [Container Adaptors](#container-adaptors)
            - [Stacks](#stacks)
            - [Queues](#queues)
            - [Priority Queues](#priority-queues)
        - [Associative Containers](#associative-containers)
            - [Sets](#sets)
            - [Multisets](#multisets)
            - [Maps](#maps)
            - [Multimaps](#multimaps)
        - [Unordered Associative Containers](#unordered-associative-containers)
        - [Pairs](#pairs)
    - [STL Algorithms](#stl-algorithms)
        - [Non-Modifying Operations](#non-modifying-operations)
            - [For Each](#for-each)
                - [For Each _n_](#for-each-_n_)
            - [Find Algorithms](#find-algorithms)
                - [Find](#find)
                - [Find If](#find-if)
                - [Find End](#find-end)
                - [Find First Of](#find-first-of)
                - [Adjacent Find](#adjacent-find)
            - [Count and Count If](#count-and-count-if)
            - [Equal and Mismatch](#equal-and-mismatch)
            - [Search and Search N](#search-and-search-n)
            - [Test Algorithms](#test-algorithms)
                - [All Of](#all-of)
                - [Any Of](#any-of)
                - [None Of](#none-of)
                - [Is Permutation](#is-permutation)
        - [Modifying Operations](#modifying-operations)
            - [Transform](#transform)
            - [Copy and Move Functions](#copy-and-move-functions)
                - [Copy](#copy)
                - [Copy Backward](#copy-backward)
                - [Copy If](#copy-if)
                - [Copy N](#copy-n)
                - [Move (Range)](#move-range)
                - [Move Backward](#move-backward)
            - [Swap](#swap)
            - [Replace](#replace)
            - [Fill](#fill)
            - [Generate](#generate)
            - [Remove](#remove)
            - [Unique and Unique Copy](#unique-and-unique-copy)
            - [Reverse and Reverse Copy](#reverse-and-reverse-copy)
            - [Rotate](#rotate)
            - [Shuffle](#shuffle)
        - [Sorting and Partitioning Operations](#sorting-and-partitioning-operations)
            - [Sort](#sort)
            - [Partial Sort](#partial-sort)
            - [Nth Element Sort](#nth-element-sort)
            - [Sort Checking](#sort-checking)
            - [Partition](#partition)
        - [Other Operations](#other-operations)
            - [Binary Searching](#binary-searching)
            - [Merge Operations](#merge-operations)
                - [Merge](#merge)
                - [In-place Merge](#in-place-merge)
                - [Includes](#includes)
                - [Set Algorithms](#set-algorithms)
            - [Heap Operations](#heap-operations)
            - [Min/Max](#minmax)
            - [Permutations](#permutations)
            - [Lexicographical Comparison](#lexicographical-comparison)
- [Modern C++](#modern-c)
    - [R-Value References](#r-value-references)
        - [Move Semantics](#move-semantics)
        - [Perfect Forwarding](#perfect-forwarding)
    - [Lambda Expressions](#lambda-expressions)
        - [Lambda Expression Components](#lambda-expression-components)
        - [Generic Lambdas](#generic-lambdas)
    - [Smart Pointers](#smart-pointers)
        - [Unique Pointers](#unique-pointers)
            - [Moving a Unique Pointer](#moving-a-unique-pointer)
        - [Shared and Weak Pointers](#shared-and-weak-pointers)
    - [Function Adapters](#function-adapters)
        - [Bind and Placeholders](#bind-and-placeholders)
        - [mem_fn](#mem_fn)
    - [Assertions and Type Traits](#assertions-and-type-traits)
        - [static_assert](#static_assert)
        - [typeid](#typeid)
        - [Primary Type Traits](#primary-type-traits)
        - [Composite Type Categories](#composite-type-categories)
        - [Type Properties](#type-properties)
        - [Type Features](#type-features)
        - [Type Relationships](#type-relationships)
        - [Property Queries](#property-queries)
    - [String View](#string-view)
    - [Optional Values](#optional-values)
    - [Attributes](#attributes)
- [Upcoming Features (C++20)](#upcoming-features-c20)
- [Miscellaneous Features](#miscellaneous-features)
    - [Placement `new`](#placement-new)
- [Threading and Multi-tasking](#threading-and-multi-tasking)
    - [Creating and Joining Threads](#creating-and-joining-threads)
    - [Mutexes](#mutexes)
    - [Locks](#locks)
        - [Lock Guards](#lock-guards)
        - [Unique Locks](#unique-locks)
            - [Unique Lock Tags](#unique-lock-tags)
            - [Unique Lock Functions](#unique-lock-functions)
        - [Shared Locks](#shared-locks)
    - [Condition Variables](#condition-variables)
    - [Future and Promise](#future-and-promise)
    - [Packaged Tasks](#packaged-tasks)
    - [Async](#async)
    - [Thread Local Objects](#thread-local-objects)
    - [Atomic Variables](#atomic-variables)
- [Additional Libraries](#additional-libraries)
    - [Boost](#boost)
    - [Qt](#qt)
    - [GTK+](#gtk)
    - [OpenCV](#opencv)
    - [Crypto++](#crypto)
    - [ZLib](#zlib)
    - [OpenSSL](#openssl)
    - [POCO](#poco)
    - [STXXL](#stxxl)
- [C++ Toolchain](#c-toolchain)
    - [Compilers](#compilers)
        - [GCC](#gcc)
        - [Clang](#clang)
        - [MSVC](#msvc)
        - [Others](#others)
    - [Working With Libraries](#working-with-libraries)
        - [Static Library Creation](#static-library-creation)
        - [Dynamic/Shared Library Creation](#dynamicshared-library-creation)
        - [Library Linking](#library-linking)
    - [Build Tools](#build-tools)
        - [Make](#make)
        - [Ninja](#ninja)
        - [CMake](#cmake)
        - [Meson](#meson)
        - [Other Build Tools](#other-build-tools)
    - [Testing Frameworks](#testing-frameworks)
        - [Google Test](#google-test)
        - [Boost Test](#boost-test)
        - [Microsoft Testing Framework](#microsoft-testing-framework)
        - [Catch2](#catch2)
        - [QuickBench](#quickbench)

<!-- /TOC -->

## Classes

The main feature separating C++ from languages like C is its built-in support of classes. Classes are a component of Object-Oriented Programming that allow for multiple variables, functions, and properties ([constructors](#constructors), [destructors](#destructors), and [operator overloads](#operator-overloading)) to be organized into new types. These new types can also be used as the base for other classes in a process known as inheritance.

A typical C++ class might look like this:

```C++
class Person
{
private:
    int age;
public:
    std::string Name;

    int GetAge()
    {
        return age;
    }
}
```

### Access Modifiers

Access modifiers control which classes/functions can access a certain variable or function. C++ supports three access modifiers:

| Access Level | Description |
|---|---|
| `private` | Can only be accessed by functions within the same instance of the class |
| `protected` | Can only be accessed by functions within the same class or a class that is derived from this class |
| `public` | Can be accessed by any function |

**Note:** While C++ only supports these three access modifiers, the [`friend` keyword](#friend) can provide some more granular control over which classes/functions can access what.

### Constructors

A constructor is essentially a function that creates an instance of a class. A constructor must be called before a class object can be used. A class can have multiple constructors, each with its own input parameters. A constructor should typically set up a class for use and initialize all member variables.

An example of a class with a constructor might look like:

```C++
class MyClass
{
private:
    int number;
    std::string name;

public:
    MyClass(int n, std::string s)
    {
        number = n;
        name = s;
    }
};
```

**Note:** even if a class does not have a constructor written for it, that class can still be constructed because the compiler creates what is known as a default constructor (a constructor that takes no parameters and does nothing other than create a new instance of the class). The default constructor can be overridden or omitted using [`default` or `delete`](#default--delete)

#### Initialization Lists

While the syntax above is correct, the C++ language provides an alternate method of assigning parameters to variables in a constructor. This can be faster in some cases and separates the assignment code from the calculation/manipulation code of a constructor.

```C++
MyClass(int n, std::strings) : number(n), name(s)
{
    // do other stuff here
}
```

### Destructors

Destructors are similar to constructors as they are inherent functions of a class. Rather than create a new instance of a class, however, a destructor disposes the class instance. Like the constructor, the destructor has a default created by the compiler that simply disposes of the class instance, and typically this default destructor is fine for classes. If your class has dynamically allocated memory, however, you will want to create a destructor that deletes any and all allocated pointers.

Example:

```C++
class MyClass
{
private:
    LargeClass* pLC;

public:
    int* listInt;

    MyClass(int* nums, int sz)
    {
        listInt = new int[sz];

        for (int i = 0; i < sz; i++)
        {
            listInt[i] = nums[i];
        }

        pLC = new LargeClass();
    }

    ~MyClass()
    {
        delete pLC;
        delete[] listInt;
    }
};
```

As seen in the example above, if the destructor did not delete `pLC` and `listInt`, a large object and `sz` number of ints would be abandoned on the heap, leading to a memory leak every time a `MyClass` object was disposed.

### Default / Delete

Using `= default`, a constructor can be explicitly assigned to the compiler generated copy, or move, or trivial constructor. `= delete` explicitly prevents the compiler from creating these constructors.

- `= delete` can also be used to prevent a member function with particular parameters.

### Explicit

The `explicit` specifier prevent implicit conversion in constructors.

### Friend

`friend` can be used to allow a class' `private` and `protected` members/methods to be accessed by a designated class, class method, or global function. The `friend` must be declared in the class to be shared.

```C++
class Class1
{
private:
    void DoSomething();

    friend void Class2::GoWithFriend();
    friend class Class3;
};

class Class2
{
private:
    void DoMore()
    {
        Class1::DoSomething();  // compiler error as DoSomething is private
    }
public:
    void GoWithFriend()
    {
        Class1::DoSomething();  // OK as GoWithFriend is friend of Class1
    }
}

class Class3
{
private:
    void DoEvenMore()
    {
        Class1::DoSomething();  // OK as Class3 is friend of Class1
    }
}
```

### "Interfaces"

While other languages have interfaces built-in to the language, C++ implements the concept of interfaces through abstract classes and (multiple) inheritance. To create an "interface", an abstract class must be created. An abstract class is a class with one or more "pure `virtual`" functions (`virtual` functions assigned to 0). An example of an abstract class would be as follows:

```C++
class Shape
{
    virtual int GetArea() = 0;
    virtual int GetPerim() = 0;

    Shape();
    virtual ~Shape();
}
```

This class cannot be instantiated as it has `null` defined functions. It can, however be inherited to adapt common function names and have them overridden. An "interface" can also be inherited along with a base class to provide the interfacing functionality.

### Copy Semantics

Copy semantics are used to create an object with identical properties to another. Copy constructors are implemented by creating a constructor that takes a reference of the same type as a parameter:

```C++
// Example using initializer list
Object(const Object& o) : var1(o.var1), var2(o.var2) {}
```

Copy assignment operators can also be used to create new objects by overloading the `=` operator:

```C++
Object& operator=(const Object& o)
{
    if (this != &o)
    {
        var1 = o.var1;
        var2 = o.var2;
    }
    return *this;
}
```

### Conversion Semantics

Conversion semantics handle creating a new object given an input of another object of a different type. A simple example of this would be creating a new `float` from an `int`. Like copy semantics, conversion semantics can be utilized using a constructor or an assignment operator.

```C++
FloatObj& FloatObj(const IntObj& o) : var1(o.var1 + 0.0f), var2(o.var2 + 0.0f) {}
```

## C++ Keywords

### Const

| Keyword            | Description                                                                                                                 |
| ------------------ | --------------------------------------------------------------------------------------------------------------------------- |
| `const` (value)    | Value that cannot be changed at runtime.                                                                                    |
| `const` (function) | Member function that cannot change the class object at runtime.                                                             |
| `constexpr` <!--C++11-->       | Function that must be calculated at compile-time, or a `const` variable that must be initialized at compile-time, for substitution. |

### Mutable

`mutable` is used to indicate that a member variable _can_ be modified inside a `const` member function. This has some limited use such as allocation and locking/unlocking mutexes in `const` member functions.

### Inline

`inline` functions are expanded in place when called during compile time. This eliminates the overhead of creating a function call and parameter values on the stack.

- Best for class method definitions in header files when they are short (~1-3 lines)

**Note:** as of C++17, variables can be made `inline` as well

### Extern

`extern` indicates that a function or variable is declared somewhere else, but allows defining it externally. The compiler will not throw an error, but if the linker cannot find the variable, it will fail.

```C++
// header.h
extern int myInt;
```

```C++
// source.cpp
#include "header.h"

int myInt;

int main()
{
    myInt = 5;
    return 0;
}
```

`extern` can also be used around a scope to make it explicitly compatible for linking, using a language specifier string ("C" or "C++"):

```C++
extern "C"
{
    ...
}
```

### Static

The `static` keyword affects the lifetime and storage of a variable/function.

- `static` variables are only stored once, meaning that they retain there value even after leaving their scope.
- `static` class members exist only once for a class. Each instance shares these variables. Cannot be initialized in a constructor.
- `static` objects (classes) exist for the lifetime of the program, they are not disposed when they fall out of scope.
- `static` class methods exist only once for a class. `static` methods can only refer to `static` class members or other `static` methods. These methods should be accessed by the scope resolution operator, `::`.

### Volatile

Using the `volatile` keyword indicates to the compiler that a variable's value is not solely within a program's scope. It is to be always accessed from RAM at usage to ensure it hasn't changed as a result of another process.

### Auto and Decltype
<!--C++11-->

Modern C++ supports type inference. When writing code, the type that should be used can sometimes become ambiguous. By using an inferred type, the compiler can decide what type to use. One way to do this is with the `auto` type. This typename simply evaluates the use of the variable and then substitutes the appropriate typename. Another way to do this is using the `decltype` specifier. `decltype` evaluates an expression and then assigns a type based on the evaluation.

```C++
int sum(int a, int b)
{
    return a + b;
}

decltype(sum(4, 5)) myVar;  // typename evaluates to the return type of sum (int)
```

## Templates

Templates allow for easy extension of the C++ language and avoid writing multiple functions or classes with similar behaviors. Template allow generic parameters and types within their body, but can also be specialized for performance or edge-cases.

### Template Functions

One of the most common uses for templates is creating a generic function. These functions can take a generic type (typically denoted by _T_ or _T1_, _T2_, etc.) as an argument and/or return type. The generic type is replaced at compile time by the actual type(s) used for the specific call of the function.

An example of a template function might look like:

```C++
template <typename T>   // Specifies the generic type name(s) to be used
T Avg(T var1, T var2)   // Function that takes two parameters of type T and returns a value of type T
{
    return (var1 + var2) / 2;
}
```

This function can be called in the following way:

```C++
int n1 = 5;
int n2 = 15;
float f1 = 2.5;
float f2 = 4.5;

float fAvg = Avg(f1, f2);   // 3.5
int iAvg = Avg(n1, n2); // 10
```

So even though we need two functions (average two `int` and average two `float`), we only have to type one template function.

### Template Classes

The other use of templates is creating a generic class (or struct). This can be particularly useful for creating container-type classes (in fact, the STL uses templates to implement its containers like `vector`)

An example template class might look like:

```C++
template <typename T>
class Rectangle
{
private:
    T length, width;

public:
    T Area, Perimeter;

    Rectangle(T len, T wid) : length(len), width(wid)
    {
        Area = length * width;
        Perimeter = length * 2 + width * 2;
    }
};
```

This class could be used like this:

```C++
Rectangle<int> rect1(4, 5);
Rectangle<float> rect2(6.5, 5.0);

std::cout << rect1.Area; // 20
std::cout << rect2.Perimeter; // 23
```

Note the angle brackets indicating the type to be used by the template class. This is not necessary for template functions as the compiler can determine the appropriate type, but for classes it must be specified.

Again, instead of creating multiple classes, a single template can be created and used in different ways.

As mentioned earlier, multiple generic types can be used by templates. A quick example of this could look like:

```C++
template <typename T1, typename T2>
class ExPair
{
public:
    T1 first;
    T2 second;

    ExPair(T1 var1, T2 var2) : first(var1), second(var2) {}
};
```

This class could then be used like so:

```C++
ExPair<int, std::string> pair1(4, "Hello");
ExPair<float, float> pair2(4.55, 8.32);

std::cout << pair1.first << ", " << pair1.second << std::endl; // 4, Hello
std::cout << pair2.first << ", " << pair2.second << std::endl; // 4.55, 8.32
```

### Template Specialization

While fully generic templates are very useful, sometimes certain implementations of a template need to be different or restricted. This can be done using a C++ feature known as template specialization. The behavior of a specific use case of a template can be modified, while the generic version is left alone for all other use cases.

#### Specialized Template Functions

Here is an example of a template function being specialized for the case of the parameter being of the type `int`.

```C++
template <typename T>
void func(T var)
{
    std::cout << var << " is a generic type" << std::endl;
}

template <>
void func(int var)
{
    std::cout << var << " is an integer" << std::endl;
}
```

In the example above, if an integer is passed into the template function `func`, it will have a unique message, while any other type will print the generic message.

#### Specialized Template Classes

Specialization can also be used for classes:

```C++
template <typename T>
class MyClass
{
private:
    std::vector<T> list;

public:
    void PrintAll()
    {
        for (auto& v : list)
        {
            std::cout << v << std::endl;
        }
    }

    void PrintTotal()
    {
        T sum;
        for (auto& v : list)
        {
            sum += v;
        }

        std::cout << sum << std::endl;
    }

    void AddVal(T val)
    {
        list.push_back(val);
    }
};

template <>
class MyClass <std::string>
{
private:
    std::vector<std::string> list;
public:
    void PrintAll()
    {
        for (auto& v : list)
        {
            std::cout << v << std::endl;
        }
    }
    // No PrintTotal method generated when value is string

    void AddVal(std::string val)
    {
        list.push_back(val);
    }
};
```

### C++ Metaprogramming With Templates
<!-- TODO: -->

## Other C++ Features

### Dynamic Memory

Like C and many other compiled languages, C++ allows for the dynamic allocation, assignment, re-allocation, and de-allocation of memory. While C++ supports using C's `malloc` and `free` functions, a more friendly way of handling dynamic memory is the default for all C++ programs. This comes with the `new` and `delete` operators.

#### The `new` operator
<!-- TODO: -->
#### The `delete` operator
<!-- TODO: -->

**Note:** while learning the `new` and `delete` operators are important, it should be said that the C++ standards committee is trying to push [smart pointers](#smart-pointers) as the preferred way of using dynamic memory.

### References

In addition to pointers, C++ provides another way to use variables _by reference_ using references. References are an r-value that refers to a variable, but is not an actual pointer object. References are not mapped to memory like pointers, and are immutable unlike pointers. References are denoted using the `&` notation. References are most useful for passing variables to functions without copying the variable into memory.

```C++
MyFunc(std::vector<int>& vec. int num)
{
    vec.push_back(num);

    for (auto n : vec)
    {
        std::cout << n << std::endl;
    }
}

int main()
{
    std::vector<int> MyVec = { 1, 4, 5, 7 };
    //...
    MyFunc(MyVec, 11);
}
```

### Operator Overloading
<!-- TODO: -->
### Functors
<!-- TODO: -->

### Enum Classes/Structs
<!--C++11-->

A special type of `enum` exists in C++, known as an `enum class` (or `struct`). An `enum class` is type-safe, being strongly typed and strongly scoped, but can behave like a standard `enum`.

```C++
// Example of enum collision
enum Color
{
    RED = 1,
    ORANGE = 2,
    BLUE = 3
};

enum Fruit
{
    BANANA = 1,
    APPLE = 2,
    ORANGE = 3
};

SomeFunc(ORANGE); // which ORANGE is used, will it be 2? 3?
```

```C++
// enum classes fix this
enum class Color
{
    RED = 1,
    ORANGE = 2,
    BLUE = 3
};

enum class Fruit
{
    BANANA = 1,
    APPLE = 2,
    ORANGE = 3
};

SomeFunc(Color::ORANGE); // scope is required, so we know it is 2
```

An `enum class` can also specify a storage size like so:

```C++
enum class Language : unsigned char
{
    AFRIKAANS = 0x01U,
    ARABIC = 0x02U,
    BULGARIAN = 0x3U,
    // ...
};
```

### Exceptions
<!-- TODO: -->

#### Noexcept
<!--C++11-->

The keyword `noexcept` can be used in a function signature to indicate to the compiler that the function _should_ not throw any exceptions. This can lead to optimizations for other functions and classes. This indication is on the programmer, not the compiler, however. A `noexcept` function that does throw an exception will compile, but at run-time will execute `std::terminate()` when the exception is thrown, which can be dangerous and should be avoided at all cost.

`noexcept` can also be used as an operator that evaluates an expression to see if it may throw an exception and returns a bool indicating if it does not.

## STL

The Standard Template Library for C++ (STL). Is a collection of libraries that typically come bundled with C++ compilers or SDKs and are compatible with most operating systems and compilers. The STL, as its name suggests, consists of mostly templates which allows its classes and functions to be used by almost any data type. The goal of the STL is to provide a standardized and optimized way of using common data types and performing common operations in C++.

### STL Containers

The main feature the STL provides is its containers. Containers are template classes that organize other data types and provide specific functions for creating, modifying, accessing, and manipulating the data of the containers. Containers also allow programmers to pass a collection of data to functions and restrict the way the data is used.

#### Sequence Containers

Sequence containers are containers that store there elements in a specific order, and can be accessed using an index, or some other defined access function.

##### Vectors

Vectors are essentially dynamic arrays. They can be initialized with a certain number of elements (or none) and after that, elements can be added and removed. Vectors can be accessed sequentially or randomly and support appending elements to the end or inserting them in the middle. Vectors are the most popular and compatible STL container and should be used when there are no specific requirements for storage, access, or modification.

```C++
std::vector<int> vec;

vec.push_back(5);
vec.push_back(10);
vec.push_back(15);

std::cout << vec[2]; // 15

auto it = vec.begin() + 1;
vec.insert(it, 8); // inserts 8 as 2nd element

it = vec.begin() + 2;
vec.erase(it); // erases 3rd element (10)

for (auto n : vec)
{
    std::cout << n << " ";  // 5 8 15
}
```

##### Lists

A list is an STL container class with pointers to the previous and next elements in the list (aka doubly-linked list). Lists are very efficient for sequential access, but are slow otherwise as they don't support true random access.

##### Forward Lists

Forward lists are similar to lists but they only have pointers to the next element in the list (singly-linked list). They are slightly more efficient for sequential access than a list, but are limited to only moving in one direction.

##### Deques

Deque is shorthand for a double-ended queue. Deques allow for adding or removing elements at the beginning or end of the container and do support random access (though they have more overhead than vectors).

##### STL Arrays

While C++ does support C-style arrays (aka `[]`), the C++ standards try to encourage use of containers. The `std::array<T>` template class provides a fixed-size container just like C arrays, but provides most of the member functions of other sequence containers for additional usefulness.

##### Bitsets

Bitsets allow for storing a sequence of bits and manipulating them using unary operators and other functions. Bitsets are a specialized class that allows for optimized storage size and allocation.

##### Valarrays

A valarray is similar to a `std::array<T>` but it is restricted to arithmetic and pointer types (see [type traits](#assertions-and-type-traits)). This allows it to perform mathematical operations on the whole array or even subsets of the array.

#### Container Adaptors

Container adaptors are classes that act like sequence containers, but are actually classes that are based on existing container classes with some specializations. Regular sequence containers can be "adapted" and transformed into one of these classes without data loss.

##### Stacks

Stacks are specialized [deques](#deques) that only allow adding (pushing) or accessing/removing (popping) from the "top". This is known as a LIFO (Last In, First Out) system.

##### Queues

Queues are specialized [deques](#deques) that only allow adding to the back, and accessing/removing from the front. This is known as a FIFO (First In, First Out) system.

##### Priority Queues

Priority queues behave like queues (FIFO system), but a priority queue ensures that the top element is the greatest based on a comparator function. Therefore, the element with the highest determined "priority" rises to the top. Priority queues are typically based on vectors, but can be overridden to be based on deques if your priority queue needs to grow rapidly (vectors take up less space, but require using slower random access inserters)

#### Associative Containers

Associative containers allow storing and accessing their elements by associated values, rather than their position in the container. Ordered associative containers use "red/black" binary trees to store data and therefore are sorted by default.

Associative containers are modified primarily using their `find`, `insert`, and `erase` functions.

##### Sets

A set is a container of elements, where each element must be unique. It can be useful when extracting unique values out of a dataset for example, as duplicates will be ignored.

##### Multisets

Multisets are sets that allow for duplicate keys. While multisets may seem counter-intuitive, there are a few specific uses for them. For example, compared to sequential containers, finding an element in a multiset is much faster.

##### Maps

Maps contain a series of key-value pairs. The value of an element in a map can be accessed by looking-up its unique key. This is essentially the same as dictionary.

##### Multimaps

Multimaps are maps that allow non-unique keys. The can be useful for creating a list of objects that have certain properties.

#### Unordered Associative Containers

Unordered associative containers access data by using a unique identifier and a hash function, rather than a tree. Hashed values are distributed among "buckets" for easy searching, though growing an unordered associative container can be costly. Unordered associative containers cannot be accessed in order, but can provide constant time allocation and access. Unordered versions of every associative container are available (`std::unorderded_set`, `std::unordered_map`, etc.)

#### Pairs

Pairs are relatively simple containers that contain two elements of the same or different types. Both elements are stored together and can be accessed using either the `first` or `second` member variable.

### STL Algorithms

The STL provides algorithms that operate on STL containers and their derivatives. These algorithms provide a common interface for STL containers. While not every container can be used in every algorithm, the common naming and underlying implementation keep things consistent and intuitive.

#### Non-Modifying Operations

##### For Each

Using `std::for_each`, a function can be applied to all values in a range. The function is passed to `std::for_each` with a functor (or lambda)

```C++
void plus1(int n)
{
    return n + 1;
}

std::vector<int> v { 55, 75, 123, 66 };

std::for_each(v.begin(), v.begin() + 2, plus1);

std::for_each(v.begin(), v.end(), [](int n) { std::cout << n << " "; });
// 56 76 123 66
```

###### For Each _n_
<!--C++17-->

A variation of `std::for_each`, `std::for_each_n` applies a function to _n_ number of values from the beginning of a range. This has the same effect as the first `std::for_each` from above (rewritten with a lambda):

```C++
std::vector<int> v { 55, 75, 123, 66 };

std::for_each_n(v.begin(), 2, [](auto& n) { n += 1; });
```

##### Find Algorithms

The find family of algorithms search within a range for a given value and return an iterator pointing to a match, or the `end()` iterator if none are found.

###### Find

The `std::find` algorithm looks for the first occurrence of a value.

```C++
void printIter(const std::vector<int>& vec, const std::vector<int>::iterator& it)
{
    if (it != vec.end())
    {
        std::cout << it - vec.begin() << std::endl;
    }
    else
    {
        std::cout << "Not found" << std::endl;
    }
}

std::vector<int> v { 12, 664, 66, 454, 78, 66 };

std::vector<int>::iterator it1 = std::find(v.begin(), v.end(), 66);
printIter(v, it1); // 2

auto it2 = std::find(v.begin(), v.end(), 82);
printIter(v, it2); // Not found
```

###### Find If

The `std::find_if` algorithm looks for the first element that causes a passed-in predicate to return `true`. A predicate is a function that operates on an element and returns a `bool`. Predicates should have clear names such as "IsOdd" or "HasFriend".

```C++
bool IsWhiteSpace(const char& c)
{
    return (c == ' ' || c == '\t' || c == '\n' || c =='\r');
}

std::string s = "The dog is brown";

auto it1 = std::find_if(s.begin(), s.end(), IsWhiteSpace);
++it1;  // Move iterator past the whitespace
auto it2 = std::find_if(it1, s.end(), IsWhiteSpace);

std::for_each(it1, it2, [](char c) { std::cout << c; }); // dog
```

As of C++11, `std::find_if_not` also exists and behaves exactly like `std::find_if` except that it returns and element when the predicate is `false`.

###### Find End

The `std::find_end` algorithm returns an iterator to the first element of the last occurrence of a range within a range. Words are confusing, here is an example:

```C++
std::string s = "This is a long sequence of text and not everything is important"
                "so I'm sure you won't want to read the whole thing"
                "so here is the important part, hello";

std::string kw = "important";
auto it = std::find_end(s.begin(), s.end(), kw.begin(), kw.end());

std::for_each(it, s.end(), [](char c) { std::cout << c; });
// important part, hello
```

###### Find First Of

The `std::find_first_of` algorithm returns an iterator to the first element in a range that matches any of the elements in another range.

```C++
std::vector<int> v = { 4, 12, 17, 6, 3, 9 };
std::vector<int> primes = { 2, 3, 5, 7, 11, 13, 17, 19 };

auto it = std::find_first_of(v.begin(), v.end(), primes.begin(), primes.end());

std::cout << *it; // 17
```

###### Adjacent Find

The `std::adjacent_find` algorithm returns an iterator pointing to the first element where an adjacent element matches it using a predicate (with `==` as the default)

```C++
bool IsOneLess (int i, int j)
{
    return i == (j - 1);
}

std::vector<int> v = { 4, 6, 6, 3, 4, 1, 2, 2 };

auto it1 = std::adjacent_find(v.begin(), v.end());
std::cout << *it1 << std::endl; // 6

auto it2 = std::adjacent_find(v.begin(), v.end(), IsOneLess);
std::cout << *it2 << std::endl; // 3
```

##### Count and Count If

The `std::count` and `std::count_if` algorithms return a number value representing the number of matches in a range.  `std::find` matches based on a given value, while `std::count_if` matches based on a predicate.

```C++
bool IsAnimal(std::string str)
{
    return (str == "Dog" || str == "Cat" || str == "Horse" || str == "Cow");
}

std::vector<std::string> v = { "Dog", "Cat", "Gary", "Horse",
                                "Dog", "Cow", "Horse", "Dog" };

auto dogCount = std::count(v.begin(), v.end(), "Dog"); // 3
auto animalCount = std::count_if(v.begin(), v.end(), IsAnimal); // 7
```

##### Equal and Mismatch

The `<algorithm>` header provides ways to compare entire ranges as well. The `std::equal` algorithm returns a boolean indicating whether the elements in two ranges are equal. Additionally, the `std::mismatch` algorithm returns a `std::pair` of iterators for the first position where two ranges differ. Each of these default to equality, but can be provided with a predicate.

```C++
int nums1[] = { 10, 20, 30, 40, 50 };
int nums2[] = { 10, 20, 30, 40, 50 };
int nums3[] = { 10, 20, 30, 41, 51 };

if (std::equal(nums1, nums1 + 5, nums2, nums2 + 5))
{
    std::cout << "nums1 and nums2 are equal" << std::endl;
}

if (std::equal(nums1, nums1 + 5, nums3, nums3 + 5))
{
    std::cout << "nums1 and nums3 are equal" << std::endl;
}
else
{
    auto mmPair = std::mismatch(nums1, nums1 + 5, nums3, nums3 + 5);
    std::cout << "where nums1 has " << *mmPair.first
        << ", nums2 has " << *mmPair.second << std::endl;
}
// nums1 and nums2 are equal
// where nums1 has 40, nums2 has 41
```

##### Search and Search N

The `std::search` algorithm is essentially the same as [`std::find_end`](#find-end) except that it returns the first match instead of the last. Using the same example:

```C++
std::string s = "This is a long sequence of text and not everything is important"
                "so I'm sure you won't want to read the whole thing"
                "so here is the important part, hello";

std::string kw = "important";
auto it = std::search(s.begin(), s.end(), kw.begin(), kw.end());

std::for_each(s.begin(), it, [](char c) { std::cout << c; });
// This is a long sequence of text and not everything is
```

The `std::search_n` on the other hand, is similar to [`std::adjacent_find`](#adjacent-find), except that the value and count of that value can be specified.

```C++
std::vector<int> v = { 4, 6, 6, 3, 4, 6, 6, 6, 2, 2 };

auto it1 = std::search_n(v.begin(), v.end(), 3, 6);
std::cout << it1 - v.begin() << std::endl; // 5
```

##### Test Algorithms

Test algorithms test an entire range and return a boolean indicating the test result. These algorithms all come with C++11.

###### All Of
<!--C++11-->

Tests every value of a given range against a predicate and returns `true` if every element returns `true` from the predicate.

###### Any Of
<!--C++11-->

Tests every value of a given range against a predicate and returns `true` if at least one element returns `true` from the predicate.

###### None Of
<!--C++11-->

Tests every value of a given range against a predicate and returns `true` if none of the elements return `true` from the predicate.

###### Is Permutation
<!--C++11-->

Tests whether one range is the same as another range ignoring the order of the elements.

```C++
bool IsOdd(int n)
{
    return (n % 2);
}

std::vector<int> v1 = { 1, 2, 3, 4, 5 };
std::vector<int> v2 = { 2, 4, 6, 8, 10 };
std::vector<int> v3 = { 1, 5, 4, 3, 2 };

std::cout << std::boolalpha << std::all_of(v1.begin(), v1.end(), IsOdd); // false
std::cout << std::boolalpha << std::none_of(v2.begin(), v2.end(), IsOdd); // true
std::cout << std::boolalpha <<
        std::is_permutation(v1.begin(), v1.end(), v3.begin(), v3.end()); // true
```

#### Modifying Operations

##### Transform

The `std::transform` algorithm applies a transforming function to a range of elements. A transforming function inputs a type, modifies the input, and returns a value of the same type.

```C++
int doub(int n)
{
    return n * 2;
}

std::vector<int> v = { 1, 2, 5, 6 };

std::transform(v.begin(), v.end(), v.begin(), doub);

printVector(v); // 2 4 10 12
```

##### Copy and Move Functions

While containers can be copied using an assignment or copy constructor, this is not compatible when the container types differ.It also does not allow for partial copies. The `std::copy` and related functions provide solutions for this.

###### Copy

`std::copy` takes two iterators as a range, and one iterator as the start of the output. **Note:** the container being copied to must have _at least_ the same number of elements as the range you are copying already initialized. Additionally, FIFO and LIFO containers (i.e. [Stacks](#stacks) and [queues](#queues)) will not work as they cannot be pre-allocated.

```C++
std::vector<int> v = { 4, 6, 12, 14 };
std::list<int> l(4);

std::copy(v.begin(), v.end(), l.begin());
printVector(v); // 4, 6, 12 ,14
```

###### Copy Backward

Another similar function is `std::copy_backward`. Though it is named somewhat confusingly, the `std::copy_backward` function simply provides a way to copy elements from the end moving inward, rather than outward. (for a way to copy a container in reverse order, see [reverse copy](#reverse-and-reverse-copy)) **Note:** this will not work on any container without backward or bidirectional iterators such [forward lists](#forward-lists).

```C++
std::vector<int> v = { 4, 6, 12, 14 };

v.resize(v.size() + 3); // allocate three more ints on the end of v

printVector(v); // 4 6 12 14 0 0 0

// move first three to the end, moving inward in reverse order
std::copy_backward(v.begin(), v.begin() + 3, v.end());

printVector(v); // 4 6 12 14 4 6 12
```

###### Copy If
<!--C++11-->

Copies values in a given range that match a predicate to the output iterator.

```C++
bool IsOdd(int n)
{
    return (n % 2);
}

bool IsEven(int n)
{
    return !IsOdd(n);
}

std::vector<int> all = { 1, 2, 3, 4, 5, 6 };
std::vector<int> odd(3), even(3);

std::copy_if(all.begin(), all.end(), odd.begin(), IsOdd);
std::copy_if(all.begin(), all.end(), even.begin(), IsEven);
```

###### Copy N
<!--C++11-->

Copies the first _n_ elements from a range.

```C++
std::vector<int> v = { 1, 2, 3, 4, 5, 6 };
std::vector<int> v2 = { 11, 22, 33, 44, 55, 66 };

std::copy_n(v.begin(), 4, v2.begin());

printVector(v2); // 1 2 3 4 55 66
```

###### Move (Range)
<!--C++11-->

With the introduction of [rvalue references](#rvalue-references) and [move semantics](#move-semantics) in C++11, an STL function to move elements was also introduced to take advantage of these new features. The `std::move` function moves elements from one range/container to another, with the previous location containing "an unspecified but valid state" in place of the element that was moved.

**Note:** the `std::move` function in this case is actually an overloaded function that operates on ranges, it should not be confused with the more [traditional use](#move-semantics) of `std::move` that calls the move constructor for objects (in fact, the move function for ranges calls the move object function).

**Also Note:** As POD types (int, char, etc.) do not have constructors or destructors, they cannot be moved and therefore, a `std::move` on a POD object or a range of POD objects (`vector<int>`, etc.) will have the same effect as a `std::copy`.

```C++
std::vector<std::string> v = { "1", "2", "3", "4", "5" };
std::vector<std::string> v2(3);

std::move(v.begin(), v.begin() + 3, v2.begin());

printVector(v);  // _ _ 4 5

printVector(v2);  // 1 2 3
```

###### Move Backward
<!--C++11-->

The `std::move_backward` function is essentially the [`std::copy_backward`](#copy-backward) function with move semantics instead of copy semantics.

##### Swap

The `std::swap` function swaps the values of two variables.

```C++
std::vector<int> vec = { 1, 2, 3, 5, 4, 6 };

std::swap(vec[3], vec[4]);

printVector(vec); // 1 2 3 4 5 6
```

##### Replace

The `std::replace` algorithm finds all values that match in a range and replace theme with a different value.

```C++
std::vector<int> vec = { 1, 3, 6, 4, 2, 33, 4 };

std::replace(vec.begin(), vec.end(), 33, 3);

printVector(vec); // 1 3 6 4 2 3 4
```

`std::replace_if` operates similarly to `std::replace`, but uses a predicate instead of a value.

`std::replace_copy` operates similarly to `std::replace`, but copies the replaced copy to an iterator position.

##### Fill

The `std::fill` algorithm simply fills a range with a value.

```C++
std::vector<int> vec = { 1, 3, 5, 7, 9, 11 };
std::fill(vec.begin(), vec.begin() + 4, 2);
std::fill(vec.begin() + 4, vec.end(), 3);

printVector(vec); // 2 2 2 2 3 3
```

##### Generate

The `std::generate` function operates somewhat similar to `std::fill`, but instead of assigning a single value to a range, it assigns the output of a "generator function" (a parameterless function that returns a value that can be assigned to the iterator) to a range. Typically this is used in conjunction with a sequence or random number generator.

```C++
int RandomNum()
{
    return std::rand() % 100;
}

// unique number generating functor
struct uniqueN
{
    int val = 0;
    int operator() ()
    {
        return val++;
    }
} UniqueNum;

std::vector<int> randVec(8);
std::vector<int> uniqVec(8);
std::generate(randVec.begin(), randVec.end(), RandomNum);
std::generate(uniqVec.begin(), uniqVec.end(), UniqueNum);

printVector(randVec); // 83 86 77 15 93 35 86 92
printVector(uniqVec); // 0  1  2  3  4  5  6  7
```

##### Remove

The `std::remove` algorithm removes objects in a range that match the given value. **Note:** this does not automatically erase the object removed, it simply shifts all values over and appends values on the end (see example).

```C++
std::vector<int> vec = { 1, 2, 3, 17, 4, 5 };
std::vector<int> vec2 = vec;

// extra value on the end
std::remove(vec.begin(), vec.end(), 17);
printVector(vec); // 1 2 3 4 5 5

// erased value
vec2.erase(std::remove(vec2.begin(), vec2.end(), 17), vec2.end());
printVector(vec2); // 1 2 3 4 5
```

`std::remove_if` operates similarly to `std::remove`, but uses a predicate instead of a value.

`std::remove_copy` operates similarly to `std::remove`, but copies the removed copy to an iterator position.

##### Unique and Unique Copy

The `std::unique` algorithm eliminates _consecutive_ duplicate values in a range in a manner very similar to `std::remove`. For this reason, the values are shifted and not erased explicitly. **Note:** as this is based on consecutive values, a `std::sort` should be done first.

```C++
std::vector<int> vec = { 1, 4, 7, 4, 2, 4, 1, 6};
std::sort(vec.begin(), vec.end());
vec.erase(std::unique(vec.begin(), vec.end()), vec.end());

printVector(vec); // 1 4 7 2 6
```

`std::unique_copy` copies the unique version to an iterator position.

##### Reverse and Reverse Copy

The `std::reverse` algorithm simply reverses the order of a range of elements.

```C++
std::string str = "Hello";
std::string palindrome = "racecar"; // I couldn't resist the opportunity!

std::reverse(str.begin(), str.end());
std::reverse(palindrome.begin(), palindrome.end());

std::cout << str << std::endl; // olleH
std::cout << palindrome << std::endl; // racecar
```

`std::reverse_copy` copies the reversed version to an iterator position.

##### Rotate

The `std::rotate` algorithm shifts elements in a range with a wrap around.

`std::rotate_copy` copies the shifted version to an iterator position.

```C++
std::vector<int> vec = { 1, 2, 3, 4, 5 };
std::vector<int> vec2(5);
std::vector<int> vec3(5);

std::rotate(vec.begin(), vec.begin() + 1, vec.end()); // shift 1 to left
std::rotate_copy(vec.begin(), vec.begin() + 2, vec.end(), vec2.begin()); // shift 2 more to left
std::rotate_copy(vec.rbegin(), vec.rbegin() + 1, vec.rend(), vec3.rbegin()); // shift back 1 to the right

printVector(vec); // 2 3 4 5 1
printVector(vec2); // 4 5 1 2 3
printVector(vec3); // 1 2 3 4 5
```

##### Shuffle
<!--C++11-->

The `std::shuffle` algorithm rearranges the elements in a range using a random generator.

```C++
auto rng = std::default_random_engine();
std::vector<int> vec = { 1, 2, 3, 4, 5 };
std::shuffle(vec.begin(), vec.end(), rng);

printVector(vec); // possible output: 3 1 5 4 2
```

#### Sorting and Partitioning Operations

The sorting and partitioning algorithms in the STL have a pretty obvious result: they reorder the elements based on a set of conditions. While they don't add or delete any elements, the container is altered by these algorithms.

##### Sort

It might seem obvious, but the `std::sort` algorithm sorts a range of elements. It has a default sorting comparison, but a comparison function can also be provided to specify the sorting technique.

```C++
bool LastNameComp(std::string name1, std::string name2)
{
    auto it1 = std::find(name1.begin(), name1.end(), ' ') + 1;
    auto it2 = std::find(name2.begin(), name2.end(), ' ') + 1;
    std::string last1 = std::string(it1, name1.end());
    std::string last2 = std::string(it2, name2.end());

    return last1 < last2;
}

std::vector<int> nVec = { 2, 5, 3, 1, 8 };
std::vector<std::string> names = { "Bill Smith", "Jackson Jones", "Wilma Flintstone", "Alison Adams" };

std::sort(nVec.begin(), nVec.end());
std::sort(names.begin(), names.end(), LastNameComp);

printVector(nVec); // 1 2 3 5 8
printVector(names); // Alison Adams Wilma Flintstone Jackson Jones Bill Smith
```

`std::stable_sort` is similar to `std::sort` except that the original relative order is maintained.

##### Partial Sort

The `std::partial_sort` algorithm sorts a subset of a given range and leaves the remainder alone. This is done by providing a middle iterator that indicates the point where sorting is no longer required.

```C++
bool GreaterThan(int n1, int n2)
{
    return n1 > n2;
}

std::vector<int> vec = { 100, 44, 123, 77, 86, 101, 72, 92, 57 };
std::partial_sort(vec.begin(), vec.begin() + 5, vec.end(), GreaterThan); // only care about top 5

printVector(vec); // 123 101 100 92 86 44 72 77 57
```

##### Nth Element Sort

The `std::nth_element` algorithm sorts a range of elements such that only the specified "nth" element is sorted. All other values are simply partitioned around the nth element based on the comparison.

```C++
std::vector<int> vec = { 2, 5, 6, 3, 4, 1, 7 };
std::nth_element(vec.begin(), vec.begin() + 4, vec.end()); // sort on 4th element

printVector(vec); // 2 1 3 _4_ 5 6 7
```

##### Sort Checking
<!--C++11-->

The STL also provides algorithms to determine whether a range is already sorted. The `std::is_sorted` algorithm returns a `bool` indicating whether a specified range is sorted in ascending order and can be passed in a comparison function. The `std::is_sorted_until` algorithm returns the number of sorted elements starting from the beginning of the range.

##### Partition

The `std::partition` algorithm sorts a range of elements such that the elements that return `true` for a predicate appear before those that return `false`. **Note:** the order of elements is not preserved.

```C++
bool IsAnimal(std::string s)
{
    return (s == "dog" || s == "cat" || s == "bird" || s == "fish");
}

std::vector<std::string> things = { "umbrella", "sock", "dog", "water", "fish", "house", "cat" };

std::partition(things.begin(), things.end(), IsAnimal);

printVector(things); // cat fish dog water sock house umbrella
```

If the relative order of elements needs to be preserved, use the slightly slower `std::stable_partition` (syntax is the same as `std::partition`).

#### Other Operations

##### Binary Searching

Using `std::binary_search` searches a range for a value and returns `true` if that value exists in that range. A binary search optimizes the number of comparisons made by using the position of elements in the range. This requires that the range is sorted or partitioned with respect to the value that is being searched for.

```C++
std::vector<std::string> strVec = { "dog", "cat", "frog", "pig", "cow" };
std::sort(strVec.begin(), strVec.end());

std::string val = "horse";

if (std::binary_search(strVec.begin(), strVec.end(), val))
{
    std::cout << val << " found." << std::endl;
}
else
{
    std::cout << "No " << val << " in range." << std::endl; // No horse in range.
}
```

Another set of algorithms that are optimized with binary searching are the `std::lower_bound` and `std::upper_bound` algorithms. `std::lower_bound` returns an iterator to the first element in a range that is **not** less than a given value, while `std::upper_bound` returns an iterator to the first element in a range that is greater than a given value.

```C++
std::vector<int> vec = { 10, 20, 30, 30, 40, 50 };

auto lower = std::lower_bound(vec.begin(), vec.end(), 30) - vec.begin(); // 2
auto upper = std::upper_bound(vec.begin(), vec.end(), 30) - vec.begin(); // 4
```

Another binary searching algorithm is the `std::equal_range` which returns a subrange of elements that equal the specified value. This is equivalent to creating a `std::pair` of the `std::lower_bound` and `std::upper_bound`. (Remember, binary search algorithms depend on the operating range being sorted or partitioned)

##### Merge Operations

The STL `<algorithm>` header also provides ways of merging ranges into one.

###### Merge

The most obvious algorithm to do this is `std::merge`. `std::merge` takes two sorted ranges and moves them into a new range indicated by an iterator that wil contain the first element of the new merged and sorted range.

```C++
std::vector<int> vec1 = { 1, 5, 9, 3, 7, 11 };
std::vector<int> vec2 = { 6, 8, 2, 4, 10, 0 };
std::vector<int> vec_merged(vec1.size() + vec2.size());

std::sort(vec1.begin(), vec1.end());
std::sort(vec2.begin(), vec2.end()); // remember to sort!

std::merge(vec1.begin(), vec1.end(), vec2.begin(), vec2.end(), vec_merged.begin());

printVector(vec_merged); // 0 1 2 3 4 5 6 7 8 9 10 11
```

###### In-place Merge

The `std::inplace_merge` algorithm, allows for "merging" a range in two chunks. The end result is similar to a `std::stable_sort` but with less complexity as the two chunks are already sorted.

```C++
std::vector<int> vec1 = { 2, 5, 22, 7, 15, 4 };
std::vector<int> vec2 = { 8, 15, 21, 3, 18, 2 };
std::vector<int> vec3(vec1.size() + vec2.size());

std::sort(vec1.begin(), vec1.end());
std::sort(vec2.begin(), vec2.end());

auto it = std::copy(vec1.begin(), vec1.end(), vec3.begin());
std::copy(vec2.begin(), vec2.end(), it);

std::inplace_merge(vec3.begin(), vec3.begin() + vec1.size(), vec3.end());

printVector(vec3); // 2 2 3 4 5 7 8 15 15 18 21 22
```

###### Includes

The `std::includes` algorithm returns `true` if all of the values in a sorted range can be found in another sorted range. This is essentially a subset test.

```C++
std::set<std::string> fruits = { "banana", "apple", "orange", "lemon", "lime", "watermelon" };
std::set<std::string> citrus = { "orange", "lemon", "lime" };

std::sort(fruits.begin(), fruits.end());
std::sort(citrus.begin(), citrus.end());

if (std::includes(fruits.begin(), fruits.end(), citrus.begin(), citrus.end()))
{
    std::cout << "Citrus is a subset of fruits" << std::endl;   // true
}

if (std::includes(citrus.begin(), citrus.end(), fruits.begin(), fruits.end()))
{
    std::cout << "Fruits is a subset of citrus" << std::endl;   // false
}
```

###### Set Algorithms

The STL also provides merge operations that operate based on [set theory](https://en.wikipedia.org/wiki/Set_theory).

- `std::set_union` : constructs a sorted range that contains the set union of two sorted ranges
- `std::set_intersection` : constructs a sorted range that contains the set intersection of two sorted ranges
- `std::set_difference` : constructs a sorted range that contains the set difference of two sorted ranges
- `std::set_symmetric_difference` : constructs a sorted range that contains the set symmetric difference of two sorted ranges

##### Heap Operations
<!-- TODO: -->
##### Min/Max
<!-- TODO: -->
##### Permutations
<!-- TODO: -->
##### Lexicographical Comparison
<!-- TODO: -->

## Modern C++

The following features have been introduced in the C++11, C++14, or C++17 specifications. While ~90% of compilers support C++11, it is still considered _"modern C++"_, as it offers a different way of programming and could be considered its own _dialect_ of C++.

### R-Value References
<!--C++11-->

_rvalue_ references are denoted by using `&&` instead of `&`. _rvalue_ references differ from _lvalue_ references in that _lvalue_ references **cannot** be bound to temporary variables (aka _rvalues_). They are useful for move semantics.

#### Move Semantics
<!--C++11-->

Move semantics may seem similar to copy and conversion semantics, but there is an important difference: move semantics involve using _rvalue_ references and deleting the original object. Moving objects can be very efficient as it avoids the overhead of copying each element. This is especially useful for handing off pointers as the old pointer is cleared. Move constructors are created taking an _rvalue_ reference of the same type as an input:

```C++
Object(Object&& o) : ptr1(o.ptr1), ptr2(o.ptr2)
{
    // Set old pointers to NULL
    o.ptr1 = nullptr;
    o.ptr2 = nullptr;
}
```

Move assignment operators can also be used:

```C++
Object& operator=(Object&& o)
{
    if (this != &o)
    {
        std::swap(ptr1, o.ptr1);
        std::swap(ptr2, o.ptr2);
    }
    return *this;
}
```

An example of using a move constructor:

```C++
Object A("Foo", "Bar");
Object B = std::move(A);

std::cout << A.Str1 << " " << A.Str2 << std::endl;  // {blank}
std::cout << B.Str1 << " " << B.Str2 << std::endl;  // Foo Bar
```

#### Perfect Forwarding
<!--C++11-->

Another use of _rvalue_ references is the use of so called "perfect forwarding". Function forwarding is the simple practice of passing a function's parameter(s) to another function. This can cause issues with references when used in templates, however, as the first template function interprets `T` as an object, and therefore cannot forward parameters that are *rvalues*. With _rvalue_ references, however, templates can now forward by reference using the `std::forward` function.

```C++
void g(T&& v)
{
    std::cout << "rvalue" << std::endl;
}

void g(T& v)
{
    std::cout << "lvalue" << std::endl;
}

template<typename T>
void f(T&& v)
{
    g(std::forward<T>(v));
}

int main()
{
    Object x;
    f(x);   // "lvalue"
    f(Object()); // "rvalue"
}
```

### Lambda Expressions
<!--C++11-->

Lambda expressions can be thought of inline, anonymous, generic functions/functors. They can be used when a functor or function-like operations are needed, but will not be reused. A lambda follows the following pattern:

```C++
[/*capture clause*/](/*parameters*/) /*mutable*/ /*exception*/ -> /*type*/ { /*body*/ }
```

#### Lambda Expression Components

- Capture Clause
    - Determines how the lambda interacts with other variables within the same scope.
    - Comma separated list of 0 or more variables
    - Variables can be explicitly captured by name (think whitelist)
        - Using a `&` before the variable name indicates it is to be used by reference
    - All variables in scope can also be captured using a general statement
        - Statements can either be a single `=` to capture all by value or a single `&` to capture all by reference
    - Explicit captures can be appended to general statements provided they are of the opposite type
        - If `=` is used, variables with `&` can be used to explicitly capture them by reference, or if `&` is used, variables without `&` can be used to explicitly capture by value
    - As of C++14, the capture clause can contain expression to assign or modify captured variables
- Lambda Parameters
    - Lambda parameters are exactly like function parameters
- Mutable Keyword (*optional*)
    - Lambdas are treated as `const` by default. Using `mutable` prevents this.
- Exception specifier (*optional*)
    - Can specify a `noexcept` or `throw()`
- Trailing Return Type (*optional*)
    - Explicitly sets the return type of a lambda
- Body
    - Lambda body code is exactly like function body code

#### Generic Lambdas
<!--C++14-->

As of C++14, lambdas can be made generic using the `auto` keyword. This makes them functionally similar to template functions (though less verbose and more useful for low use). This is done by specifying the return type and one or more of the parameters as `auto`.

```C++
auto l_Add = [](auto n1, auto n2) { return n1 + n2; };

std::cout << "float: " << l_Add(0.5f, 0.7f); // float: 1.2
std::cout << "int: " << l_Add(5, 4); // int: 9
```

### Smart Pointers
<!--C++11-->

With C++11, a new way to manage memory was introduced, smart pointers. Smart pointers wrap a standard pointer into a container template to add functionality such as reference counting and move semantics. The STL provides three varieties of smart pointers, the `unique_ptr`, `shared_ptr`, and `weak_ptr`.

#### Unique Pointers

The `unique_ptr` automatically handles the deletion of the pointed object. Deletion occurs when the `unique_ptr` falls out of scope, is reassigned, or by calling the `reset` function. As the name suggests, a `unique_ptr` is unique, only one instance can exist at a time and it cannot be shared. While a `unique_ptr` can be allocated like a regular pointer, as of C++14, a new, recommended way exists: `make_unique`.

```C++
// dynamically allocates new instance of MyClass
std::unique_ptr<MyClass> u_ptr = std::make_unique<MyClass>();

// Unique pointers can be accessed using the same operators as raw pointers
u_ptr->DoSomething();

// The get() function returns a pointer to the object of a smart pointer
MyClass* rw_ptr = u_ptr.get();

std::unique_ptr<MyClass> bad_ptr = u_ptr; // Won't compile as this would make u_ptr non-unique
```

##### Moving a Unique Pointer

Since a `unique_ptr` cannot be copied, a move operator must be used instead:

```C++
std::unique_ptr<MyClass> u_ptr2 = std::move(u_ptr);
```

**Note:** `u_ptr` is now removed and invalid

#### Shared and Weak Pointers

The `shared_ptr` adds reference counting to a pointer. When a `shared_ptr` is created, its reference count is initialized to 1, and every time another `shared_ptr` is created pointing to the same object, its count is incremented and vice versa. If the reference count reaches 0, the `shared_ptr` is deleted.

The `weak_ptr` is used in conjunction with shared pointers. A `weak_ptr` can point to the same object as a `shared_ptr`, but it does not affect the reference count of the `shared_ptr`. A `weak_ptr` will not, however prevent the object from being destroyed, so if a shared pointer's reference count reaches 0, the `weak_ptr` will become invalid.

```C++
std::shared_ptr<int> sptr = std::make_shared<int>(45);  //allocate pointer to 45
std::shared_ptr<int> sptr2 = sptr;  // new shared_ptr increments ref count
std::weak_ptr<int> wptr(sptr); // create weak pointer pointing to 45

std::cout << sptr.use_count();  // 2, as weak_ptr doesn't count

sptr2.reset();

std::cout << sptr.use_count();  // 1

if (std::shared_ptr<int> tmp = wptr.lock()) // checks if weak_ptr is valid
{
    std::cout << *tmp;  // 45
}

sptr.reset();

std::cout << sptr.use_count();  // 0

if (std::shared_ptr<int> tmp = wptr.lock()) // weak_ptr is no longer valid
{
    std::cout << *tmp;
}
```

### Function Adapters
<!--C++11-->

Function adapters allow the transformation of functions to suit a particular need. This can have the advantage of avoiding retyping a function when only changing minor details such as the arguments or scope.

#### Bind and Placeholders
<!--C++11-->

Introduced in C++11, `std::bind` provides functionality similar to lambda expressions, but allows the functions to be reused. With bind, a function can be transformed based on provided arguments. A trivial example would be:

```C++
double divide(double x, double y)
{
    return x / y;
}

auto divide10By2 = std::bind(divide, 10, 2);
double myDoub = divide10by2();  // Always 5
```

This doesn't have a whole lot of use on its own, but can be enhanced using the `std::placeholders` namespace. Placeholders allow inputs to bound functions. Placeholders are denoted by an underscore followed by a number indicating the argument they are replaced by.

```C++
auto divideBy2 = std::bind(divide, std::placeholders::_1, 2);
double myDoub2 = divideBy2(22);  // 11
```

**Note:** `std::bind` does have significant overhead and more times than not, using lambdas or `std::function` should be preferred.

#### mem_fn
<!--C++11-->

Normally, member functions of classes cannot be called by outside functions or objects, but as long as the member function is not instance-specific (i.e. can be made static), it can be converted to a standard function using the `std::mem_fn` function.

```C++
class MyClass
{
    int ClassFunc(int i);
}

auto regFunc = std::mem_fn(&MyClass::ClassFunc);
int myInt = regFunc(4);
```

### Assertions and Type Traits

#### static_assert
<!--C++11-->

By using a `static_assert` (from the `<type_traits>` header), expressions can be evaluated by the compiler, and will fail with a notification if the expression(s) fail.

```C++
static_assert(2 == 2, "Everything is broken!");
// If 2 didn't equal 2, the compiler would fail and tell you that
// "Everything is broken!" because this should never happen ;)
```

Another (more practical) example (see [Primary Type Traits](#primary-type-traits) for more on `is_integral`):

```C++
template<class T>
T Sum(T x, T y)
{
    static_assert(is_integral<T>::value, "Can only sum integral types!");
    return x + y;   // Compiler will fail if passed in a float or string
}
```

#### typeid

Type information can be obtained at runtime. One way to do this is using the `typeid` operator. The `typeid` can be compared directly:

```C++
double decimal1 = 1.01;
float decimal2 = 2.22;

if (typeid(decimal1) == typeid(decimal2)) // false
```

And can be converted to (`const char*`) strings:

```C++
std::cout << typeid(decimal1).name(); // Prints "d" or "double"
```

#### Primary Type Traits
<!--C++11-->

Template type traits exist in `<type_traits>` to determine if a variable meets certain type requirements. These are known as primary type traits as they are based off of the built-in types of C++.

The primary type traits consist of:

| Type Trait                   | Requirements                                                            |
| ---------------------------- | ----------------------------------------------------------------------- |
| `is_array`                   | A `[]` variable (but not a pointer to an array)                         |
| `is_class`                   | A `class` or `struct` (not an `enum` or `union`)                        |
| `is_enum`                    | An `enum` or `enum` `class`/`struct`                                    |
| `is_floating_point`          | `float` or `double`                                                     |
| `is_function`                | function or function template (not lambdas or () operators)             |
| `is_integral`                | `short`, `int`, `long`, `long long` `unsigned` (also `char` and `bool`) |
| `is_lvalue_reference`        | is a `&` reference                                                      |
| `is_member_function_pointer` | pointer to a member function                                            |
| `is_member_object_pointer`   | pointer to a member object                                              |
| `is_pointer`                 | pointer to anything (`*`, `**`, etc.)                                   |
| `is_rvalue_reference`        | is a `&&` reference                                                     |
| `is_union`                   | is a `union`                                                            |
| `is_void`                    | is `void` (`void*` doesn't count as it is a pointer)                    |

The boolean value of a type trait is accessed by its value property.

```C++
double myDoub = 0.4;
static_assert(std::is_integral<decltype(myDoub)>::value, "not an int"); // fails
```

#### Composite Type Categories
<!--C++11-->

Composite type categories combine multiple primary type traits based on the operations that can be done with them.

| Type Category       | Requirements                                                                    |
| ------------------- | ------------------------------------------------------------------------------- |
| `is_arithmetic`     | integer or floating point                                                       |
| `is_compound`       | non-fundamental types (`class`, `struct`, `enum`, function, pointer, reference) |
| `is_fundamental`    | arithmetic, `void`, or `nullptr`                                                |
| `is_member_pointer` | pointer to member                                                               |
| `is_object`         | non-void, non-function, non-reference types                                     |
| `is_reference`      | `&` and `&&` references                                                         |
| `is_scalar`         | arithmetic, pointers, member pointers, `enum`, and `nullptr`                    |

#### Type Properties

Another group of type traits are known as type properties. These have to do with the runtime and compiler properties of a type.

| Type Property           | Requirements                                                                                    |
| ----------------------- | ----------------------------------------------------------------------------------------------- |
| `is_abstract`           | has a pure `virtual` function                                                                   |
| `is_const`              | is `const` qualified (includes `constexpr`)                                                     |
| `is_empty`              | contains no non-`static` members, `virtual` functions, or `virtual` base classes                |
| `is_literal_type`       | can be `constexpr`. Includes scalar, reference, or simple `class`/`struct`                      |
| `is_pod`                | C compatible `struct` and `class` (no constructors or member functions)                         |
| `is_polymorphic`        | has a `virtual` function or inherits one                                                        |
| `is_signed`             | arithmetic type where -1 < 0                                                                    |
| `is_standard_layout`    | scalar types and classes and structs with certain restrictions                                  |
| `is_trivial`            | scalar types and classes and structs with certain restrictions                                  |
| `is_trivially_copyable` | scalar types and objects that have implicit copy and move constructors and assignment operators |
| `is_unsigned`           | arithmetic type where -1 > 0                                                                    |
| `is_volatile`           | type qualified with `volatile`                                                                  |

#### Type Features
<!--C++11-->

Type features are another subset of type traits that are based on the features of objects. Some type features can take multiple arguments for their template (such as `is_constructible`)

| Type Feature                         | Requirements                                    |
| ------------------------------------ | ----------------------------------------------- |
| `has_virtual_destructor`             | Has virtual destructor                          |
| `is_assignable`                      | Is assignable                                   |
| `is_constructible`                   | Is constructible                                |
| `is_copy_assignable`                 | Is copy assignable                              |
| `is_copy_constructible`              | Is copy constructible                           |
| `is_destructible`                    | Is destructible                                 |
| `is_default_constructible`           | Is default constructible                        |
| `is_move_assignable`                 | Is move assignable                              |
| `is_move_constructible`              | Is move constructible                           |
| `is_trivially_assignable`            | Is trivially assignable                         |
| `is_trivially_constructible`         | Is trivially constructible                      |
| `is_trivially_copy_assignable`       | Is trivially copy assignable                    |
| `is_trivially_copy_constructible`    | Is trivially copy constructible                 |
| `is_trivially_destructible`          | Is trivially destructible                       |
| `is_trivially_default_constructible` | Is trivially default constructible              |
| `is_trivially_move_assignable`       | Is trivially move assignable                    |
| `is_trivially_move_constructible`    | Is trivially move constructible                 |
| `is_nothrow_assignable`              | Is assignable throwing no exceptions            |
| `is_nothrow_constructible`           | Is constructible throwing no exceptions         |
| `is_nothrow_copy_assignable`         | Is copy assignable throwing no exceptions       |
| `is_nothrow_copy_constructible`      | Is copy constructible throwing no exceptions    |
| `is_nothrow_destructible`            | Is nothrow destructible                         |
| `is_nothrow_default_constructible`   | Is default constructible throwing no exceptions |
| `is_nothrow_move_assignable`         | Is move assignable throwing no exception        |
| `is_nothrow_move_constructible`      | Is move constructible throwing no exceptions    |

#### Type Relationships
<!--C++11-->

Another subset of type traits are type relationships, they examine the relationship between two types.

| Type Relationship | Requirements                                     |
| ----------------- | ------------------------------------------------ |
| `is_base_of`      | is class A derived from class B                  |
| `is_convertible`  | implicit conversion of A to B                    |
| `is_same`         | is A same type as B (example: is it a `typedef`) |

#### Property Queries
<!--C++11-->

Property queries are different from other type traits in that their value is equal to non-boolean values.

| Property Query | Result                                          |
| -------------- | ----------------------------------------------- |
| `alignment_of` | number of bytes in memory of the object         |
| `extent`       | number of elements in multi-dimensional array   |
| `rank`         | number of dimensions in multi-dimensional array |

### String View
<!--C++17-->

Introduced in C++17, the `std::string_view` template class allows for a non-owning, non-mutable reference to a "string" object. It involves no additional memory allocation and can increase performance in code where string are used, but do not have to be owned.

```C++
bool compStr(const std::string& s1, const std::string& s2)
{
    return s1 == s2;
}

bool compStrVw(std::string_view s1, std::string_view s2)
{
    return s1 == s2;
}

std::string str = "This is a string to compare things to";  // Memory allocated for string

compStr(str, "This is a different string to compare things to");    // Literal string has memory allocated
compStrVw(str, "This is a different string to compare things to");  // Literal string_view has no memory allocation
```

### Optional Values
<!-- TODO: -->

### Attributes
<!--C++11-->

A new feature to C++, attributes are implementation-defined tags that can be used in various places in C++ code. Here are a few examples:

- `[[noreturn]]`: indicates that the function does not return to the calling function (either by throwing, or by exiting, etc.)
- `[[deprecated]]` <!--C++14-->: indicates that a function or entity is allowed but discouraged (i.e. could be removed in future version). A message can also be passed using the syntax, `[[deprecated("reason")]]`
- `[[fallthrough]]` <!--C++17-->: indicates that a case label is intentionally falling into the following case label (i.e. no `break` or `return`)
- `[[nodiscard]]` <!--C++17-->: tells the compiler to issue a warning or error if the return value is not used

_Note:_ each compiler will have its own set of attributes as well such as `[[gnu::used]]`

## Upcoming Features (C++20)
<!-- TODO: -->

## Miscellaneous Features

### Placement `new`
<!--Extra-->

In addition to the standard use of the [`new` operator](#the-new-operator), a little known variation exists, known as the "placement `new`".

With the standard use of `new`, memory is allocated to an object and that object is constructed, but by using the placement variation, each of these operations can be controlled explicitly. This variation has the syntax of `new (address) (type) initializer`. With this, memory that has already been allocated can be re-used, or a specific location can be requested.

```C++
// standard new
auto* p1 = new int[4] { 1, 2, 3, 4 };
auto* p2 = new (p1 + 2) int[2] { 5, 6 };

for (int i = 0; i < 4; ++i)
{
    std::cout << p1[i] << '\n'; // 1 2 5 6
}
```

**Note:** when using the placement `new` variation, the `delete` operator cannot be used. Instead, the pointer should explicitly call the destructor for the object it points to

## Threading and Multi-tasking

Like any modern language, C++ provides ways to create threads and to perform multiple operations at once. Also like most languages, this can be a difficult and dangerous task. While more than one implementation of threading in C++ exists, the standard `<thread>` header is widely-used and cross-platform so that is what will be used here.

### Creating and Joining Threads
<!--C++11-->

Threads are created by declaring a `thread` object and passing in a task (function, lambda, func_object):

```C++
thread t1{func1};
```

Threads can be passed arguments by reference:

```C++
// void func2(int& n) {}
int n1 = 20;
thread t2{ func2, n1 };
```

Threads cannot have a return type, however pointers can be used to compensate for this:

```C++
// void func3(int& n, int* out)
int n2 = 30;
int* result;
thread t3{ func3, n2, &result };
```

Once created, threads will split from the main thread and run. To wait for a specific thread to finish before continuing, `join` must be called:

```C++
t1.join();
t2.join();
t3.join();
// threads are done, do something else
```

### Mutexes
<!--C++11-->

By including the `<mutex>` header, C++ can implement mutexes (mutual exclusion objects) in its threading operations. Mutexes allow code to be locked until it has been completed. Mutexes should be used in a high level of scope (global or near-global). Mutex names should be descriptive and indicate their scope.

Mutexes can be created as variables, and are locked and unlocked simply by calling the respective member function:

```C++
std::mutex g_mutex;

void func()
{
    g_mutex.lock();
    // do something
    g_mutex.unlock();
}
```

### Locks

The major danger of using mutexes comes in the form of deadlocks. Deadlocks can occur when a mutex is not unlocked. This can happen in a multitude of ways including race conditions, improper thread safety, or throwing an exception. Luckily, locks exist, essentially containers that use mutexes. The main advantage of locks is that their mutexes are unlocked in the destructor.

Locks come in two main varieties: `lock_guard` and `unique_lock`

#### Lock Guards

Lock guards are simple template classes containing mutexes. They are created just like any other container class:

```C++
std::lock_guard<std::mutex> myLock(g_mutex);
```

#### Unique Locks
<!--C++11-->

Unique locks are essentially lock guards with added functionality. They provide methods of implicitly constructing locks, locking and unlocking the mutex, delaying a mutex lock, and moving the mutex.

##### Unique Lock Tags

Unique locks can be created just like lock guards, but can also be created with certain behaviors (aka tags).

| Tag           | Description                                                            |
| ------------- | ---------------------------------------------------------------------- |
| `defer_lock`  | prevents locking of the mutex on construction (do not own)             |
| `adopt_lock`  | retains current lock state of mutex on construction (assume ownership) |
| `try_to_lock` | uses `try_lock` on construction (try to own)                           |

##### Unique Lock Functions

| Function         | Description                                                                          |
| ---------------- | ------------------------------------------------------------------------------------ |
| `lock`           | locks the mutex                                                                      |
| `try_lock`       | attempts to lock the mutex, but returns if it's not available                        |
| `try_lock_for`   | attempts to lock the mutex, returns if it's been unavailable for the specified time  |
| `try_lock_until` | attempts to lock the mutex, returns if it's unavailable at a specified point in time |
| `unlock`         | unlocks the mutex                                                                    |
| `swap`           | swaps state with another unique lock                                                 |
| `release`        | disassociates the mutex without unlocking it                                         |
| `mutex`          | returns a pointer to the mutex                                                       |
| `owns_lock`      | returns a `bool` indicating whether the lock owns its mutex                          |

#### Shared Locks
<!--C++14-->
<!-- TODO: -->

### Condition Variables
<!--C++11-->

Condition variables offer another form of synchronization between threads. While a mutex can only be locked and unlocked, a condition variable can be used to "signal" to a monitoring thread that it is ready.

Condition variables provide three wait functions:

- `wait`  : wait until notified
- `wait_for` : wait for specific amount of time or until notified
- `wait_until` : wait until specific time or until notified

And two notify functions:

- `notify_all` : notifies all waiting threads
- `notify_one` : notifies one waiting thread (essentially at random)

Example:

```C++
std::mutex g_mtx;
std::condition_variable g_cv;
bool ready = false;

void getReady(int i)
{
    std::cout << i << " is ready!" << std::endl;
    std::unique_lock<std::mutex> lck(g_mtx);
    while (!ready) g_cv.wait(lck);
    std::cout << i << " is finished!" << std::endl;
}

void go()
{
    std::unique_lock<std::mutex> lck(g_mtx);
    ready = true;
    g_cv.notify_all();
}

int main()
{
    std::thread t1{ getReady, 1 };
    std::thread t2{ getReady, 2 };

    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Get Set!" << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "GO!" << std::endl;

    go();

    t1.join();
    t2.join();

    return 0;
}
```

### Future and Promise
<!--C++11-->

`future` and `promise` provide yet another method of synchronization, specifically at the variable level. By creating a `promise` / `future` pair, we can "promise" that a variable's value will be set and wait for that to happen.

Example:

```C++
void printN(std::future<int>& f_int)
{
    int n = f_int.get(); // wait until value is set
    std::cout << n << std::endl;
}

int main()
{
    std::promise<int> p_int;
    std::future<int> f_int = p_int.get_future();

    std::thread t1 (printN, std::ref(f_int));

    p_int.set_value(30); // now printN can print

    t1.join();

    return 0;
}
```

### Packaged Tasks
<!--C++11-->

A `packaged_task` is essentially a `promise` for a function. A `future` can be created from a `packaged_task` and using the `get()` function, the function in the task will be awaited.

```C++
int func(int n)
{
    return n;
}

int main()
{
    std::packaged_task<int(int)> task(func);
    std::future<int> f_int = task.get_future();

    std::thread t1 (std::move(task), 12); // no copy constructors for packaged_task
    std::cout << f_int.get() << std::endl;

    t1.join();

    return 0;
}
```

### Async
<!--C++11-->

Asynchronous functions can also be created using the `async` keyword and assigned directly to a `future`. Note, there is no need to create threads explicitly.

```C++
std::future<int> func_a = std::async(func, 5);
```

### Thread Local Objects
<!--C++11-->

By using the `thread_local` keyword, the storage duration of an object can be modified to fit within the lifetime of a thread. This can allow each thread to have a copy of a `static` or global variable.

```C++
thread_local int i = 0;

int main()
{
    auto fnc = [](int x) { i += x; std::cout << i; };
    std::thread t1(fnc, 4); // 4
    std::thread t2(fnc, 2); // 2 (w/o thread_local would be 6)
    return 0;
}
```

### Atomic Variables
<!--C++11-->

Atomic variables are guaranteed to be synchronized among threads. Atomic variables are created by wrapping a variable type in the `atomic` container.

```C++
std::atomic<int> a_int{5};
```

## Additional Libraries

### Boost
<!-- TODO: -->
### Qt
<!-- TODO: -->
### GTK+
<!-- TODO: -->
### OpenCV
<!-- TODO: -->
### Crypto++
<!-- TODO: -->
### ZLib
<!-- TODO: -->
### OpenSSL
<!-- TODO: -->
### POCO
<!-- TODO: -->
### STXXL
<!-- TODO: -->

## C++ Toolchain

### Compilers
<!-- TODO: -->
#### GCC
<!-- TODO: -->
#### Clang
<!-- TODO: -->
#### MSVC
<!-- TODO: -->
#### Others
<!-- TODO: -->

### Working With Libraries

Libraries are essential for sharing code among programs and providing APIs to make certain operations easier. There are two types of libraries: static libraries (.a, .la, or .lib files) and dynamic or shared libraries (.so, .dll, or .dylib files).

- **Static libraries** are archives consisting of object (.o or .obj) files that are linked into code at compile time. After the code is compiled, the library is no longer needed as the required function(s) have already been built into the binary. This can be a problem, however, as it can make the executable rather large. Additionally, if the library is updated, the program must be recompiled against the latest version of the library to get the benefits.
- **Dynamic libraries** are essentially non-executable binary files as they are compiled. Dynamic libraries are linked by reference only when a program is compiled and the library is called when an underlying function is needed. This provides the advantage of a smaller executable and more modular design, but can lead to other issues, particularly when the proper dynamic libraries are not found. Unlike with static libraries, dynamic libraries must remain on the system for the executable to be 100% functional.

#### Static Library Creation

Static library creation varies based on compiler and/or platform. For example, builds on Windows using Visual Studio/MSVC will generate a .lib by changing a setting for the project. But using GCC or Clang on a *nix operating system (including macOS) is a little different. To create a static library, simply compile to generate object files:

```bash
g++ -c myfile.cpp
# OR
clang++ -c myfile.cpp
```

Then use the `ar` command to bundle all object files into a library:

```bash
ar rcs mylib.a *.o
```

#### Dynamic/Shared Library Creation

As with static libraries, the generation of dynamic libraries depends on the compiler and platform. Visual Studio users simply change the project output. Dynamic libraries are even more platform-independent than static libraries and as such have their own methods of creation:

For *nix systems:

```bash
g++ -fPIC -shared myfile.cpp -o mylib.so
```

Make sure the library is in a directory in the `ld` library path, such as /lib or /usr/lib, etc.

macOS:

```bash
clang++ -c myfile.cpp
# THEN
libtool -dynamic *.o -o mylib.dylib
```

#### Library Linking

Static libraries must be linked at compile-time, while dynamic libraries can be "linked" later on (i.e. at runtime) by OS-specific functions. By including a header file from the library, the linker looks for the library that contains the necessary information.

```bash
# static library
g++ myfile2.cpp -l:mylib.a
# shared library
g++ myfile2.cpp -l:mylib.so
```

### Build Tools
<!-- TODO: -->
#### Make
<!-- TODO: -->
#### Ninja
<!-- TODO: -->
#### CMake
<!-- TODO: -->
#### Meson
<!-- TODO: -->
#### Other Build Tools
<!-- TODO: -->

### Testing Frameworks

#### Google Test
<!-- TODO: -->
#### Boost Test
<!-- TODO: -->
#### Microsoft Testing Framework
<!-- TODO: -->
#### Catch2
<!-- TODO: -->
#### QuickBench
<!-- TODO: -->
